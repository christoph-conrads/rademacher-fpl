#!/usr/bin/python3

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import functools
import math
import numpy as np
import operator
import random
import scipy.special
import scipy.stats
import sys


def significand(xs):
    b_s = 52 if xs.dtype == np.float64 else 23
    dtype = np.uint64 if xs.dtype == np.float64 else np.uint32
    ss = np.abs(xs).view(dtype) & ((1<<b_s)-1)

    assert(ss.size == xs.size)

    return ss


def exponent(xs):
    b_s = 52 if xs.dtype == np.float64 else 23
    b_e = 11 if xs.dtype == np.float64 else 8
    es = (np.abs(xs).view(np.uint64) >> b_s) & ((1<<b_e)-1)

    return es


def compute_monobit_probability(xs):
    ss = significand(xs)
    n = xs.size
    k = np.sum(ss&1)
    p = scipy.stats.binom_test(k, n)

    return k/n, p


def compute_cond_dist_probability(xs):
    ss = significand(xs)
    t = ss&1 == 0

    uniform = scipy.stats.uniform(loc=0, scale=1)
    ks_0, p_0 = scipy.stats.kstest(xs[t], uniform.cdf) if np.any(t) else (0,1)
    ks_1, p_1 = scipy.stats.kstest(xs[~t], uniform.cdf) if np.any(~t) else (0,1)

    return ks_0, ks_1, p_0, p_1



def compute_monobit_correlation(xs):
    if xs.size < 32:
        return 0, 1

    ys = np.sort(xs)
    del xs

    q = 3
    n = int(np.floor(np.log2(ys.size))) - q
    offsets = np.int(2)**(np.arange(n+1))>>1
    offsets[0] = 1
    offsets *= 2**q
    offsets[-1] = ys.size - np.sum(offsets[:-1])
    indices = np.cumsum(offsets)

    ps = np.full(n, 0.0)

    for i, first in enumerate(indices[:-1]):
        last = indices[i+1]

        ss = significand(ys[first:last])
        num_odd = np.sum(ss&1)
        ps[i] = num_odd / ss.size

    # map similar percentages to similar bins
    # ensure p=0.5 is mapped to the center of a bin
    n_odd = n + 1 - n%2
    scores = np.floor(n_odd*ps)

    ranks = scipy.stats.rankdata(scores)
    tau, p_tau = scipy.stats.kendalltau(ranks, np.arange(ranks.size))

    return tau, p_tau



def main():
    #print(np.__version__)
    #print(scipy.__version__)

    tol = 1e-3
    header_fmt = '{:40s}  {:<9d}  {:s}'
    fmt = '{:40s}  {:<9.2e}  {:s}'

    for index, filename in enumerate(sys.argv[1:]):
        if filename[-11:] == 'float64.txt':
            xs = np.loadtxt(filename, dtype=np.float64)

        elif filename[-11:] == 'float32.txt':
            xs = np.loadtxt(filename, dtype=np.float32)

        elif filename[-4:] == '.txt':
            xs = np.loadtxt(filename)
            ys = xs.astype(np.float32).astype(np.float64)

            eps32 = np.finfo(np.float32).eps

            if max(abs(xs-ys)) < eps32/8:
                xs = xs.astype(np.float32)

        elif filename[-6:] == '.bin64':
            xs = np.fromfile(filename, dtype=np.float64)
        elif filename[-6:] == '.bin32':
            xs = np.fromfile(filename, dtype=np.float32)
        else:
            print('unknown file extension for file "{:s}"'.format(filename))
            continue

        print(header_fmt.format(filename, xs.size, str(xs.dtype)))

        if np.any(np.isnan(xs)):
            msg = 'dataset contains NaNs'
            print(msg)
            continue

        if min(xs) < 0 or max(xs) >= 1:
            msg = 'dataset contains values {:8.2e}, {:8.2e} outside of [0,1)'
            print(msg.format(min(xs), max(xs)))
            continue

        uniform = scipy.stats.uniform(loc=0, scale=1)
        _, p_ks = scipy.stats.kstest(xs, uniform.cdf)
        _, p_monobit = compute_monobit_probability(xs)
        _, _, p_0, p_1 = compute_cond_dist_probability(xs)
        p_cond = min(p_0, p_1)
        _, p_tau = compute_monobit_correlation(xs)

        grade = lambda p: 'pass' if np.isnan(p) or p>=tol else 'fail'

        print(fmt.format('kolmogorov-smirnov', p_ks, grade(p_ks)))
        print(fmt.format('monobit', p_monobit, grade(p_monobit)))
        print(fmt.format('conditional distribution', p_cond, grade(p_cond)))
        print(fmt.format('monobit-exponent rank correlation', p_tau, grade(p_tau)))

        if index+2 < len(sys.argv):
            print()


if __name__ == '__main__':
    main()
