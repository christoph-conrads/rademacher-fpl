#!/bin/bash

set -e
set -o pipefail
set -u

source_dir="$(pwd)"

if [ -n "${1:-}" ]
then
	build_dir="$1"
else
	now="$(date '+%Y%m%dT%H%M%S')"
	build_dir="/tmp/rademacher-fpl.$now"
	mkdir -- "$build_dir"
fi

prefix="$build_dir/prefix"


echo "build-directory $build_dir"


# this information may be buried somewhere in the logs when using continuous
# integration so just print it here
cmake --version


cd -- "$build_dir"
env CXXFLAGS="${CXXFLAGS:--fstack-protector-all}" \
	LDFLAGS="${LDFLAGS:--Wl,-z,defs -Wl,-z,now}" \
	cmake \
		${Catch2_ROOT:+"-DCatch2_ROOT=$Catch2_ROOT"} \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_INSTALL_PREFIX="$prefix" \
		-- "$source_dir"
cmake --build .
ctest --output-on-failure
cmake --build . --target install


export PKG_CONFIG_PATH="$prefix/lib/pkgconfig"

"${CXX:-c++}" \
	-Wextra -Wall -std=c++11 -pedantic ${CXXFLAGS:-} "$source_dir/example.cpp" \
	$(pkg-config --cflags --libs rademacher-fpl) \
	-o "$build_dir/example"

env LD_LIBRARY_PATH="$prefix/lib" "$build_dir/example"
