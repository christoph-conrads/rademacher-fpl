#!/bin/bash

# This script computes the line coverage of the tests using LCOV.

if ! $(lcov --help >/dev/null 2>&1)
then
	echo 'lcov not found'
	exit 1
fi

if ! $(genhtml --help >/dev/null 2>&1)
then
	echo 'genhtml not found'
	exit 1
fi

if [ ! -f build-and-test.sh ]
then
	>&2 echo 'File "build-and-test.sh" not found.'
	>&2 echo 'This script must be executed in the Rademacher FPL directory.'
	exit 1
fi


set -e
set -o pipefail
set -u

now="$(date '+%Y%m%dT%H%M%S')"
build_dir="/tmp/rademacher-fpl.$now"
rfpl_info_file="$build_dir/rfpl-coverage.info"
html_dir="$build_dir/coverage-html"
source_dir="$(pwd)"


echo "$build_dir will be used as a working directory."
echo "Building tests..."


mkdir -- "$build_dir"
env CXXFLAGS="${CXXFLAGS:-} --coverage" \
	bash build-and-test.sh "$build_dir" \
	>"$build_dir/build-and-test.log"


# for development purposes
[ -f "$rfpl_info_file" ] && rm -- "$rfpl_info_file"

find "$build_dir" -type f -name '*.info' -delete


tests="$(find "$build_dir/test" -type f -executable | sort)"

if [ -z "$tests" ]
then
	>&2 echo 'No tests found. Please check if Catch2 was found.'
	exit 1
fi

echo "Running $(wc -l <<<"$tests") tests..."

for test in $tests
do
	test_name="$(basename "$test" | awk '{ gsub("-", "_"); print $0 }')"

	find "$build_dir" -type f -name '*.gcda' -delete
	"$test" >/dev/null
	lcov \
		--quiet \
		--capture \
		--test-name "$test_name" \
		--no-external \
		--base-directory "$source_dir" \
		--directory "$build_dir" \
		--output-file "$build_dir/$test_name.info"
done

info_files="$(find "$build_dir" -type f ! -empty -name '*.info')"
tracefiles="$(awk '{printf("--add-tracefile=%s\n", $0)}' <<<"$info_files")"

lcov --quiet $tracefiles --output-file "$rfpl_info_file"

genhtml \
	--quiet \
	--show-details \
	--title 'Rademacher FPL Code Coverage Report' \
	--legend --highlight --num-spaces=4 \
	--output-directory "$html_dir" \
	-- "$rfpl_info_file"

echo "$html_dir/index.html is ready and can be opened with a webbrowser."
