// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <cstdint>
#include <rademacher-fpl/numeric.hpp>


namespace rfpl = rademacher_fpl;


TEMPLATE_TEST_CASE("gcd", "", std::uint32_t, std::uint64_t)
{
	using T = TestType;

	auto a = T{2*3*5};
	auto b = T{2*  5*7};
	auto c = T{  3*5*7};
	auto g = rfpl::gcd(a, b, c);

	CHECK(g == T{5});
}
