// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <rademacher-fpl/span.hpp>
#include <type_traits>
#include <vector>


namespace rfpl = rademacher_fpl;


TEMPLATE_TEST_CASE("simple", "", int, unsigned, float)
{
	using test_type = TestType;

	test_type xs[] = { 1, 2, 3, 4, 5 };

	auto s = rfpl::make_span(xs);

	CHECK(s.data() == xs);
	CHECK(s.size() == std::extent<decltype(xs)>::value);
	CHECK(s.size_bytes() == sizeof(xs));
}


TEMPLATE_TEST_CASE("dynamic-extent:simple", "", int, unsigned, float)
{
	using T = TestType;

	auto xs = std::vector<T>({1, 2, 3, 4, 5});
	auto s = rfpl::span<T>(xs);

	CHECK(s.data() == xs.data());
	CHECK(s.size() == xs.size());
	CHECK(s.size_bytes() == xs.size() * sizeof(T));

	const auto& ys = xs;
	auto t = rfpl::span<const T>(ys);

	CHECK(t.data() == xs.data());
	CHECK(t.size() == xs.size());
	CHECK(t.size_bytes() == xs.size() * sizeof(T));
}
