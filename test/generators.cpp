// Copyright 2018 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/generators.hpp>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;


TEST_CASE("simple.uint8")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto gen = Generator();
	auto num_draws = Generator::period_length();
		
	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(gen() == i);
	}

	CHECK(gen.num_calls() == num_draws);
	CHECK(gen() == 0);
}


TEST_CASE("simple.uint8.max")
{
	using Generator = rfpl::simple_generator<std::uint8_t, 99>;

	auto gen = Generator();
	auto num_draws = Generator::period_length();

	CHECK(num_draws == 100);

	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(gen() == i);
	}

	CHECK(gen.num_calls() == num_draws);
	CHECK(gen() == 0);
}



TEST_CASE("nested.uint8.uint8")
{
	using generator_type = rfpl::simple_generator<std::uint8_t>;
	using nested_generator_type =
		rfpl::nested_generator<generator_type, generator_type>;

	auto gen = nested_generator_type();
	(void) gen;
}
