// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cassert>
#include <catch2/catch.hpp>
#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/traits.hpp>
#include <rademacher-fpl/uniform-real-distribution.hpp>
#include <vector>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;
using rfpl::binary8;


TEST_CASE("construction")
{
	for(auto sign = 0; sign < 2; ++sign)
	{
		for(auto exponent = 0; exponent < 15; ++exponent)
		{
			for(auto significand = 0; significand < 7; ++significand)
			{
				auto f = rfpl::binary8(
					sign, exponent, significand
				);
				auto g = rfpl::assemble_like(f, sign, exponent, significand);

				CHECK(not rfpl::isnan(f));
				CHECK(not rfpl::isnan(g));
				CHECK(f == g);

				CHECK(rfpl::signbit(f) == sign);
				CHECK(rfpl::exponent(f) == exponent);
				CHECK(rfpl::significand(f) == significand);
			}
		}
	}
}


TEST_CASE("make-from-sign-exponent-pair")
{
	auto zero = binary8(0);

	CHECK(rfpl::exponent(zero) == 0);
	CHECK(rfpl::significand(zero) == 0);

	constexpr auto max_exponent =
		std::numeric_limits<binary8>::max_exponent;

	CHECK(max_exponent + rfpl::binary8::BIAS == 15);

	for(auto sign = 0; sign < 2; ++sign)
	{
		for(auto exponent = 0; exponent < max_exponent; ++exponent)
		{
			auto integer = (2 * sign - 1) * (1 << exponent);
			auto f = rfpl::binary8(integer);

			CHECK(not rfpl::isnan(f));
			CHECK(f != zero);

			CHECK(rfpl::signbit(f) == (integer >= 0 ? 0 : 1));
			CHECK(rfpl::exponent(f) == exponent + binary8::BIAS);
			CHECK(rfpl::significand(f) == 0);
		}
	}
}



TEST_CASE("make-from-integer")
{
	auto one = rfpl::binary8(1);

	CHECK(one.sign_ == 0);
	CHECK(one.exponent_ == binary8::BIAS);
	CHECK(one.fraction_ == 0);

	CHECK(rfpl::signbit(one) == 0);
	CHECK(rfpl::exponent(one) == binary8::BIAS);
	CHECK(rfpl::significand(one) == 0);

	auto two = rfpl::binary8(2);

	CHECK(two.sign_ == 0);
	CHECK(two.exponent_ == binary8::BIAS + 1);
	CHECK(two.fraction_ == 0);

	CHECK(rfpl::signbit(two) == 0);
	CHECK(rfpl::exponent(two) == binary8::BIAS + 1);
	CHECK(rfpl::significand(two) == 0);

	auto three = rfpl::binary8(3);

	CHECK(three.sign_ == 0);
	CHECK(three.exponent_ == binary8::BIAS + 1);
	CHECK(three.fraction_ == 4);

	CHECK(rfpl::signbit(three) == 0);
	CHECK(rfpl::exponent(three) == binary8::BIAS + 1);
	CHECK(rfpl::significand(three) == 4);


	for(auto integer = 9; integer <= 15; ++integer)
	{
		auto b8 = rfpl::binary8(integer);

		CHECK(b8.sign_ == 0);
		CHECK(b8.exponent_ == binary8::BIAS + 3);
		CHECK(b8.fraction_ == integer - 8);

		CHECK(rfpl::signbit(b8) == 0);
		CHECK(rfpl::exponent(b8) == binary8::BIAS + 3);
		CHECK(rfpl::significand(b8) == integer - 8);
	}


	CHECK(rfpl::binary8(16) == rfpl::binary8(17));
}



TEST_CASE("comparisons")
{
	auto f = rfpl::binary8(0, 14, 7);

	CHECK(not rfpl::isnan(f));
	CHECK(f.sign_ == 0);
	CHECK(f.exponent_ == 14);
	CHECK(f.fraction_ == 7);

	CHECK(not (f > f));
	CHECK(f >= f);
	CHECK(f == f);
	CHECK(f <= f);
	CHECK(not (f < f));

	auto g = rfpl::binary8(14);

	CHECK(not rfpl::isnan(g));
	CHECK(g.sign_ == 0);
	CHECK(g.exponent_ == 3 + rfpl::binary8::BIAS);
	CHECK(g.fraction_ == 6);

	CHECK(f  > g);
	CHECK(f >= g);
	CHECK(f != g);
	CHECK(g <= f);
	CHECK(g  < f);

	CHECK(not (g  > f));
	CHECK(not (g >= f));
	CHECK(not (g == f));
	CHECK(not (f <= g));
	CHECK(not (f  < g));

	auto inf = std::numeric_limits<rfpl::binary8>::infinity();

	CHECK(not rfpl::isnan(inf));
	CHECK(inf  > f);
	CHECK(inf >= f);
	CHECK(inf != f);
	CHECK(f   <= inf);
	CHECK(f    < inf);

	auto zero = rfpl::binary8(0);

	CHECK(not rfpl::isnan(zero));
	CHECK(rfpl::exponent(zero) == 0);
	CHECK(rfpl::significand(zero) == 0);

	CHECK(inf   > zero);
	CHECK(inf  >= zero);
	CHECK(inf  != zero);
	CHECK(zero <= inf);
	CHECK(zero  < inf);

	auto h = rfpl::binary8(1, 14, 7);

	CHECK(not rfpl::isnan(h));
	CHECK(h.sign_ == 1);
	CHECK(h.exponent_ == 14);
	CHECK(h.fraction_ == 7);

	CHECK(f  > h);
	CHECK(f >= h);
	CHECK(f != h);
	CHECK(h <= f);
	CHECK(h  < f);

	CHECK(not (h  > f));
	CHECK(not (h >= f));
	CHECK(not (h == f));
	CHECK(not (f <= h));
	CHECK(not (f  < h));
}



TEST_CASE("self-comparison")
{
	const rfpl::binary8 NUMBERS[] = {
		rfpl::binary8(0),
		rfpl::binary8(0, 14, 7),
		rfpl::binary8(14),
		rfpl::binary8(1, 4, 3),
		std::numeric_limits<rfpl::binary8>::infinity()
	};

	for(auto f : NUMBERS)
	{
		CHECK(not rfpl::isnan(f));

		CHECK(not (f > f));
		CHECK(f >= f);
		CHECK(f == f);
		CHECK(f <= f);
		CHECK(not (f < f));
	}


	auto nan = std::numeric_limits<rfpl::binary8>::quiet_NaN();

	CHECK(rfpl::isnan(nan));
	CHECK(not (nan  > nan));
	CHECK(not (nan >= nan));
	CHECK(not (nan == nan));
	CHECK(not (nan != nan));
	CHECK(not (nan <= nan));
	CHECK(not (nan  < nan));
}



TEST_CASE("not-a-number")
{
	auto nan = std::numeric_limits<rfpl::binary8>::quiet_NaN();

	CHECK(rfpl::isnan(nan));

	auto f = binary8(0, 1, 0);

	CHECK(not (f  > nan));
	CHECK(not (f >= nan));
	CHECK(not (f == nan));
	CHECK(not (f != nan));
	CHECK(not (f <= nan));
	CHECK(not (f  < nan));
}
