// Copyright 2018 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cassert>
#include <catch2/catch.hpp>
#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/generators.hpp>
#include <rademacher-fpl/impl/uniform-int-distribution.hpp>
#include <rademacher-fpl/impl/uniform-real-distribution.hpp>
#include <rademacher-fpl/test.hpp>
#include <vector>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;
using rfpl::binary8;


TEST_CASE("draw_significand_like")
{
	using Generator = rfpl::simple_generator<std::uint16_t>;

	auto a = binary8(1);
	auto gen = Generator();
	auto num_digits = rfpl::traits::num_significand_digits<binary8>::value;
	auto n = 1_z << num_digits;

	for(auto i = 0_z; i < 2*n; ++i)
	{
		auto x = rfpl::urd_impl::draw_significand_like(a, &gen);

		CHECK(x == i % n);
	}
}



TEST_CASE("draw_exponent:const")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto gen = Generator();

	auto l8 = std::uint8_t{254};
	auto r8 = std::uint8_t{255};
	auto e8 = rfpl::urd_impl::draw_exponent(l8, r8, &gen);

	CHECK(e8 == l8);

	auto l32 = std::uint32_t{51};
	auto r32 = std::uint32_t{52};
	auto e32 = rfpl::urd_impl::draw_exponent(l32, r32, &gen);

	CHECK(e32 == l32);
}


TEST_CASE("draw_exponent:simple")
{
	using unsigned_type = std::uint32_t;
	using generator_type = rfpl::simple_generator<unsigned_type>;

	auto num_bins = 8_z;
	auto bins = std::vector<std::size_t>(num_bins);
	auto e_max = std::numeric_limits<unsigned_type>::max();
	auto e_min = static_cast<unsigned_type>(e_max - num_bins);
	auto gen = generator_type();
	auto num_draws = (1_z << num_bins) - 1_z;

	for(auto i = 0_z; i < num_draws; ++i)
	{
		auto e = rfpl::urd_impl::draw_exponent(e_min, e_max, &gen);

		REQUIRE(e >= e_min);
		REQUIRE(e < e_max);

		auto k = e - e_min;

		++bins.at(k);
	}

	for(auto i = 0_z; i < bins.size(); ++i)
	{
		CHECK(bins[i] == (1_z << i));
	}
}


TEST_CASE("draw_exponent:overflow")
{
	using generator_type = rfpl::nested_generator<
		rfpl::simple_generator<std::uint8_t>,
		rfpl::simple_generator<std::uint8_t>
	>;

	auto num_bins = 9_z;
	auto bins = std::vector<std::size_t>(num_bins);
	auto e_max = std::numeric_limits<std::uint8_t>::max();
	auto e_min = static_cast<std::uint8_t>(e_max - num_bins);
	auto gen = generator_type();
	auto num_draws = (1_z << num_bins) - 1_z;

	for(auto i = 0_z; i < num_draws; ++i)
	{
		auto e = rfpl::urd_impl::draw_exponent(e_min, e_max, &gen);

		REQUIRE(e >= e_min);
		REQUIRE(e < e_max);

		auto k = e - e_min;

		++bins.at(k);
	}

	for(auto i = 0_z; i < bins.size(); ++i)
	{
		CHECK(bins[i] == (1_z << i));
	}
}
