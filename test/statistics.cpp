// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <algorithm>
#include <catch2/catch.hpp>
#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/impl/statistics.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/traits.hpp>
#include <random>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;


TEST_CASE("draw_counter:zero")
{
	auto a = rfpl::binary8(true, 0, 0);
	auto b = rfpl::binary8(false, 0, 0);
	auto xs = rfpl::draw_counter<rfpl::binary8>(a, b);

	CHECK(xs.get_left_interval_bound() == a);
	CHECK(xs.get_right_interval_bound() == b);

	auto c = rfpl::binary8(false, 0, 1);
	auto ys = rfpl::draw_counter<rfpl::binary8>(a, c);

	CHECK(ys.get_left_interval_bound() == a);
	CHECK(ys.get_right_interval_bound() == c);
	CHECK(ys.unsigned_bins_.size() > 0);
	CHECK(ys.signed_bins_.size() > 0);
}


TEST_CASE("draw_counter:alias_zero_p")
{
	auto a = rfpl::binary8(true, 0, 3);
	auto b = rfpl::binary8(false, 0, 2);
	auto alias_zero_p = true;
	auto dc = rfpl::draw_counter<rfpl::binary8>(a, b, alias_zero_p);

	CHECK(dc.get_left_interval_bound() == a);
	CHECK(dc.get_right_interval_bound() == b);
	CHECK(dc.get_subinterval(true, 0u).size() == 4);
	CHECK(dc.get_subinterval(false, 0u).size() == 2);

	auto x = rfpl::assemble_like(a, true, 0, 1);

	dc.add_draw(x);
	CHECK(dc.get_num_draws() == 1);

	auto y = rfpl::assemble_like(a, true, 0, 0);

	dc.add_draw(y, 2);
	CHECK(dc.get_num_draws() == 5);

	auto z = rfpl::assemble_like(a, false, 0, 0);

	dc.add_draw(z);
	CHECK(dc.get_num_draws() == 7);
}


TEST_CASE("draw_counter:simple")
{
	using real_t = rfpl::binary8;
	using unsigned_t = rfpl::traits::as_unsigned_t<real_t>;

	constexpr auto ONE = unsigned_t{1};
	constexpr auto NUM_EXPONENT_DIGITS =
		rfpl::traits::num_exponent_digits<real_t>::value;
	constexpr auto MAX_EXPONENT = (ONE << NUM_EXPONENT_DIGITS) - ONE;
	constexpr auto NUM_SIGNIFICAND_DIGITS =
		rfpl::traits::num_significand_digits<real_t>::value;
	constexpr auto NUM_SIGNIFICANDS = ONE << NUM_SIGNIFICAND_DIGITS;

	auto a = -std::numeric_limits<real_t>::infinity();
	auto b = +std::numeric_limits<real_t>::infinity();
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);

	for(auto sign = unsigned_t{0}; sign <= ONE; ++sign)
	{
		for(auto e = unsigned_t{0}; e < MAX_EXPONENT; ++e)
		{
			// skip -0
			for(auto s = sign==1 and e==0 ? unsigned_t{1} : unsigned_t{0};
				s < NUM_SIGNIFICANDS;
				++s
			)
			{
				auto x = rfpl::assemble_like(a, sign, e, s);

				if(s+1 < NUM_SIGNIFICANDS)
				{
					auto y = rfpl::assemble_like(a, sign, e, s+1);
					auto bin_x = draw_counter.get_bin_(x);
					auto bin_y = draw_counter.get_bin_(y);

					REQUIRE(bin_x.first + 1u == bin_y.first);
					REQUIRE(&bin_x.second == &bin_y.second);
				}

				draw_counter.add_draw(x);

				if(sign == 0)
					draw_counter.add_draw(x);
			}
		}
	}

	auto num_draws =
		count_num_values_in(a,real_t{0})
		+ 2 * count_num_values_in(real_t{0},b)
		- 1 // do not count -0
	;

	REQUIRE(draw_counter.get_num_draws() == num_draws);

	for(auto sign = unsigned_t{0}; sign <= ONE; ++sign)
	{
		for(auto e = unsigned_t{0}; e < MAX_EXPONENT; ++e)
		{
			for(auto s = sign==1 and e==0 ? unsigned_t{1} : unsigned_t{0};
				s < NUM_SIGNIFICANDS;
				++s
			)
			{
				auto r = rfpl::assemble_like(a, sign, e, s);
				auto expected_num_draws = sign == 0 ? 2u : 1u;

				CHECK(draw_counter.get_num_draws(r) == expected_num_draws);
			}
		}
	}
}


TEST_CASE("draw_counter:negative")
{
	using real_t = rfpl::binary8;
	using unsigned_t = rfpl::traits::as_unsigned_t<real_t>;

	constexpr auto ONE = unsigned_t{1};
	constexpr auto NUM_EXPONENT_DIGITS =
		rfpl::traits::num_exponent_digits<real_t>::value;
	constexpr auto MAX_EXPONENT = (ONE << NUM_EXPONENT_DIGITS) - ONE;
	constexpr auto NUM_SIGNIFICAND_DIGITS =
		rfpl::traits::num_significand_digits<real_t>::value;
	constexpr auto NUM_SIGNIFICANDS = ONE << NUM_SIGNIFICAND_DIGITS;

	auto a = real_t{-2000};
	auto b = real_t{-1};
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);
	auto compute_num_draws = [] (unsigned_t e, unsigned_t s) {
		return e + 2u*s;
	};
	auto num_draws = 0u;

	assert(rfpl::exponent(b) == real_t::BIAS);

	for(auto e = unsigned_t{real_t::BIAS}; e < MAX_EXPONENT; ++e)
	{
		for(auto s = e==real_t::BIAS ? unsigned_t{1} : unsigned_t{0};
			s < NUM_SIGNIFICANDS;
			++s
		)
		{
			auto x = rfpl::assemble_like(a, 1, e, s);
			auto m = compute_num_draws(e, s);

			if(s+1 < NUM_SIGNIFICANDS)
			{
				auto y = rfpl::assemble_like(a, 1, e, s+1);
				auto bin_x = draw_counter.get_bin_(x);
				auto bin_y = draw_counter.get_bin_(y);

				REQUIRE(bin_x.first + 1u == bin_y.first);
				REQUIRE(&bin_x.second == &bin_y.second);
			}

			for(auto i = 0u; i < m; ++i)
				draw_counter.add_draw(x);

			num_draws = num_draws + m;
		}
	}

	REQUIRE(draw_counter.get_num_draws() == num_draws);

	for(auto e = unsigned_t{real_t::BIAS}; e < MAX_EXPONENT; ++e)
	{
		for(auto s = e==real_t::BIAS ? unsigned_t{1} : unsigned_t{0};
			s < NUM_SIGNIFICANDS;
			++s
		)
		{
			auto r = rfpl::assemble_like(a, 1, e, s);
			auto expected_num_draws = compute_num_draws(e, s);

			CHECK(draw_counter.get_num_draws(r) == expected_num_draws);
		}
	}
}


// this used to be a TEMPLATE_TEST_CASE but constructing the `draw_counter`
// object with float took 1.5s. this was too slow.
TEST_CASE("draw_counter:random")
{
	using real_t = rfpl::binary8;
	using unsigned_t = rfpl::traits::as_unsigned_t<real_t>;

	constexpr auto ONE = unsigned_t{1};
	constexpr auto NUM_EXPONENT_DIGITS =
		rfpl::traits::num_exponent_digits<real_t>::value;
	constexpr auto MAX_EXPONENT = (ONE << NUM_EXPONENT_DIGITS) - ONE;
	constexpr auto NUM_SIGNIFICAND_DIGITS =
		rfpl::traits::num_significand_digits<real_t>::value;
	constexpr auto NUM_SIGNIFICANDS = ONE << NUM_SIGNIFICAND_DIGITS;

	constexpr auto E_MAX = unsigned_t{13};

	static_assert(E_MAX < MAX_EXPONENT, "");

	auto a = rfpl::assemble_like(real_t{0}, 1, E_MAX-1, 3);
	auto b = rfpl::assemble_like(real_t{0}, 0, E_MAX, 2);
	auto c = rfpl::assemble_like(real_t{0}, 0, E_MAX, 1);
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);

	constexpr auto NUM_DRAWS = 50_z << (sizeof(real_t));

	auto generator = std::mt19937();
	auto dist_exponent =
		std::uniform_int_distribution<unsigned_t>(0, E_MAX);
	auto dist_significand =
		std::uniform_int_distribution<unsigned_t>(0, NUM_SIGNIFICANDS-1);
	auto draws = std::vector<real_t>();

	draws.reserve(NUM_DRAWS);

	draw_counter.add_draw(a);
	draw_counter.add_draw(c);

	draws.push_back(a);
	draws.push_back(c);

	for(auto i = 2_z; i < NUM_DRAWS; ++i)
	{
		auto r = std::numeric_limits<real_t>::quiet_NaN();
		auto it = draws.cbegin();

		do
		{
			auto sign = i % 3 == 0 ? unsigned_t{1} : unsigned_t{0};
			auto e = dist_exponent(generator);
			auto s = dist_significand(generator);

			r = rfpl::assemble_like(real_t{0}, sign, e, s);

			auto first = draws.cbegin();
			auto last = draws.cend();

			it = std::find(first, last, r);
		} while(r < a or r >= b or it != draws.cend());

		draw_counter.add_draw(r);
		draws.push_back(r);

		REQUIRE(draw_counter.get_num_draws() == i+1);
	}

	auto first = draws.cbegin();
	auto last = draws.cend();

	for(auto it = first; it != last; ++it)
	{
		CHECK(draw_counter.get_num_draws(*it) == 1);
	}
}



TEST_CASE("draw_counter::get_subinterval")
{
	using real_t = rfpl::binary8;

	auto a = rfpl::assemble_like(real_t{0}, 1, 2, 0);
	auto b = rfpl::assemble_like(real_t{0}, 0, 1, 4);
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);

	auto xs = std::vector<real_t>({
		a,
		rfpl::assemble_like(real_t{0}, 1, 1, 7),
		rfpl::assemble_like(real_t{0}, 1, 0, 3),
		rfpl::assemble_like(real_t{0}, 1, 0, 0),
		rfpl::assemble_like(real_t{0}, 0, 0, 0),
		rfpl::assemble_like(real_t{0}, 0, 0, 3),
		rfpl::assemble_like(real_t{0}, 0, 1, 3)
	});
	auto compute_num_draws = [] (std::size_t index) { return 3*index + 1; };

	for(auto i = 0_z; i < xs.size(); ++i)
	{
		auto n = compute_num_draws(i);

		for(auto j = 0_z; j < n; ++j)
			draw_counter.add_draw(xs[i]);
	}

	for(auto i = 0_z; i < xs.size(); ++i)
	{
		auto x = xs[i];
		auto n = compute_num_draws(i);
		auto s = draw_counter.get_subinterval(
			rfpl::signbit(x), rfpl::exponent(x)
		);

		CHECK(s[rfpl::significand(x)] == n);
	}
}



TEMPLATE_TEST_CASE("compute_num_virtual_bins:simple", "", rfpl::binary8, float)
{
	using real_t = TestType;
	using uint_t = rfpl::traits::as_unsigned_t<float>;

	constexpr auto s_max = rfpl::traits::max_significand<real_t>::value;
	constexpr auto n_s = rfpl::traits::max_significand<real_t>::value + 1u;

	auto f = rfpl::compute_num_virtual_bins_r<real_t, std::uint64_t, double>;
	auto g = rfpl::compute_num_virtual_bins<real_t>;
	auto assemble = [] (uint_t sign, uint_t exponent, uint_t significand) {
		return rfpl::assemble_like(real_t{0}, sign, exponent, significand);
	};

	auto l = real_t{1};
	auto r = real_t{2};
	CHECK(f(l, r) == n_s);
	CHECK(f(l, r) == g(l, r));

	l = assemble(0, 0, n_s/2);
	r = assemble(0, 1, 0);
	CHECK(f(l, r) == n_s/2);
	CHECK(f(l, r) == g(l, r));

	l = real_t{0};
	r = assemble(0, 2, 0);
	CHECK(f(l, r) == 2*n_s);
	CHECK(f(l, r) == g(l, r));

	l = real_t{0};
	r = assemble(0, 3, 0);
	CHECK(f(l, r) == 4*n_s);
	CHECK(f(l, r) == g(l, r));

	l = real_t{0};
	r = assemble(0, 3, 0);
	CHECK(f(l, r) == 4*n_s);
	CHECK(f(l, r) == g(l, r));

	l = assemble(0, 1, 0);
	r = assemble(0, 3, 0);
	CHECK(f(l, r) == 3*n_s);
	CHECK(f(l, r) == g(l, r));

	l = assemble(0, 1, 0);
	r = assemble(0, 8, 6);
	CHECK(f(l, r) == 127*n_s + 128*6);
	CHECK(f(l, r) == g(l, r));


	l = assemble(1, 12, 3);
	r = assemble(1, 10, 5);
	CHECK(f(l, r) == 4*(3+1) + 2*n_s + (n_s-5-1));
	CHECK(f(l, r) == g(l, r));


	l = assemble(1, 0, 0);
	r = assemble(0, 0, 0);
	CHECK(f(l, r) == 1);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 0, 0);
	r = assemble(0, 0, 1);
	CHECK(f(l, r) == 2);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 3, 2);
	r = assemble(0, 0, 0);
	CHECK(f(l, r) == 4*(2+1) + 4*n_s);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 3, 2);
	r = assemble(1, 0, 0);
	CHECK(f(l, r) == 4*(2+1) + 4*n_s-1);
	CHECK(f(l, r) == g(l, r));


	l = assemble(1, 0, s_max);
	r = assemble(0, 1, 0);
	CHECK(f(l, r) == 2*n_s);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 0, s_max);
	r = assemble(0, 0, 1);
	CHECK(f(l, r) == n_s+1u);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 1, 0);
	r = assemble(0, 1, 0);
	CHECK(f(l, r) == 2*n_s + 1);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 1, 0);
	r = assemble(0, 8, 2);
	CHECK(f(l, r) == 129*n_s + 128*2 + 1);
	CHECK(f(l, r) == g(l, r));

	l = assemble(1, 4, 6);
	r = assemble(0, 3, 5);
	CHECK(f(l, r) == 8*(6+1) + 8*n_s + 4*n_s + 4*5);
	CHECK(f(l, r) == g(l, r));
}



TEMPLATE_TEST_CASE("compute_uniform_cdf:simple", "", rfpl::binary8, float)
{
	using real_t = TestType;
	using uint_t = rfpl::traits::as_unsigned_t<float>;

	constexpr auto s_max = rfpl::traits::max_significand<real_t>::value;
	constexpr auto n_s = rfpl::traits::max_significand<real_t>::value + 1u;

	auto f = rfpl::compute_uniform_cdf<real_t, std::size_t, double>;
	auto assemble = [] (uint_t sign, uint_t exponent, uint_t significand) {
		return rfpl::assemble_like(real_t{0}, sign, exponent, significand);
	};

	constexpr auto eps = std::numeric_limits<double>::epsilon();
	auto approx = [eps] (double x) { return Approx(x).margin(eps).epsilon(0); };

	auto a = real_t{1};
	auto b = real_t{2};
	CHECK(f(a, b, 0, rfpl::exponent(a)) == 1);

	a = real_t{1};
	b = real_t{4};
	CHECK(f(a, b, 0, rfpl::exponent(a)) == approx(1.0/3.0));

	a = assemble(0, 4, 2);
	b = assemble(0, 4, 5);
	CHECK(f(a, b, 0, 4) == 1);

	a = assemble(0, 0, 0);
	b = assemble(0, 4, 0);
	CHECK(f(a, b, 0, 0) == 1.0/8);
	CHECK(f(a, b, 0, 1) == 2.0/8);
	CHECK(f(a, b, 0, 2) == 4.0/8);
	CHECK(f(a, b, 0, 3) == 8.0/8);

	a = assemble(0, 1, n_s-5);
	b = assemble(0, 4, 7);
	auto c = 5 + 6*n_s + 8*7 + 0.0;
	CHECK(f(a, b, 0, 1) == approx(5/c));
	CHECK(f(a, b, 0, 2) == approx((5+2*n_s)/c));
	CHECK(f(a, b, 0, 3) == approx((5+6*n_s)/c));
	CHECK(f(a, b, 0, 4) == 1);


	a = assemble(1, 0, 0);
	b = assemble(0, 0, 0);
	CHECK(f(a, b, 1, 0) == 1);

	a = assemble(1, 0, 0);
	b = assemble(0, 0, 1);
	CHECK(f(a, b, 1, 0) == 1/2.0);
	CHECK(f(a, b, 0, 0) == 1/2.0);

	a = assemble(1, 3, s_max);
	b = assemble(0, 4, 0);
	CHECK(f(a, b, 1, 0) == 1.0/16);
	CHECK(f(a, b, 1, 1) == 2.0/16);
	CHECK(f(a, b, 1, 2) == 4.0/16);
	CHECK(f(a, b, 1, 3) == 8.0/16);
	CHECK(f(a, b, 0, 0) == 1.0/16);
	CHECK(f(a, b, 0, 1) == 2.0/16);
	CHECK(f(a, b, 0, 2) == 4.0/16);
	CHECK(f(a, b, 0, 3) == 8.0/16);

	a = assemble(1, 4, s_max);
	b = assemble(0, 4, 0);
	CHECK(f(a, b, 1, 0) == approx( 1.0/24));
	CHECK(f(a, b, 1, 1) == approx( 2.0/24));
	CHECK(f(a, b, 1, 2) == approx( 4.0/24));
	CHECK(f(a, b, 1, 3) == approx( 8.0/24));
	CHECK(f(a, b, 1, 4) == approx(16.0/24));
	CHECK(f(a, b, 0, 0) == approx( 1.0/24));
	CHECK(f(a, b, 0, 1) == approx( 2.0/24));
	CHECK(f(a, b, 0, 2) == approx( 4.0/24));
	CHECK(f(a, b, 0, 3) == approx( 8.0/24));

	a = assemble(1, 4, s_max);
	b = assemble(1, 3, s_max);
	CHECK(f(a, b, 1, 4) == 1);

	const auto max = std::numeric_limits<real_t>::max();
	const auto inf = std::numeric_limits<real_t>::infinity();

	CHECK(f(max, inf, 0, rfpl::exponent(max)) == 1);
	CHECK(f(0, inf, 0, rfpl::exponent(max)-1) == 1.0/2);
}


TEST_CASE("compute_uniform_cdf:mixed-signs")
{
	using real_t = rfpl::binary8;

	constexpr auto eps = std::numeric_limits<double>::epsilon();
	auto approx = [eps] (double x) { return Approx(x).margin(eps).epsilon(0); };

	auto f = rfpl::compute_uniform_cdf<real_t, std::size_t, double>;
	auto a = assemble_like(real_t{0}, 1, 2, 7);
	auto b = assemble_like(real_t{0}, 0, 0, 1);

	CHECK(f(a, b, 1, 0) == approx( 8/33.0));
	CHECK(f(a, b, 1, 1) == approx(16/33.0));
	CHECK(f(a, b, 1, 2) == approx(32/33.0));

}



TEST_CASE("ks-statistic:simple")
{
	using real_t = rfpl::binary8;

	constexpr auto s_max = rfpl::traits::max_significand<real_t>::value;
	constexpr auto num_significands = s_max + 1u;

	auto inf = std::numeric_limits<real_t>::infinity();

	for(auto signbit_p : {true, false})
	{
		auto a = signbit_p
			? rfpl::assemble_like(inf, true,  1, s_max)
			: rfpl::assemble_like(inf, false, 0, 0)
		;
		auto b = signbit_p
			? rfpl::assemble_like(inf, false, 0, 0)
			: rfpl::assemble_like(inf, false, 2, 0)
		;
		auto draw_counter = rfpl::draw_counter<real_t>(a, b);
		auto d_n = compute_kolmogorov_smirnov_statistic(draw_counter);

		CHECK(std::isnan(d_n));

		draw_counter.add_draw(a);

		d_n = compute_kolmogorov_smirnov_statistic(draw_counter);
		CHECK(d_n == 1-1.0/(2*num_significands));

		auto first = signbit_p ? rfpl::nextafter_0(b, a) : nextafter_0(a, b);
		auto last =  signbit_p ? a : b;

		for(auto x = first; x != last; x = nextafter_0(x, last))
			draw_counter.add_draw(x);

		d_n = compute_kolmogorov_smirnov_statistic(draw_counter);

		CHECK(d_n == 0);
	}
}



TEST_CASE("ks-statistic:small-interval")
{
	using real_t = rfpl::binary8;

	auto f = rfpl::compute_kolmogorov_smirnov_statistic<real_t>;
	auto inf = std::numeric_limits<real_t>::infinity();
	auto a = rfpl::assemble_like(real_t{0}, 0, 3, 1);
	auto b = rfpl::assemble_like(real_t{0}, 0, 3, 6);
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);

	auto d_n = f(draw_counter);
	CHECK(std::isnan(d_n));

	draw_counter.add_draw(a);

	d_n = f(draw_counter);
	CHECK(d_n == 0.8);


	for(auto r = rfpl::nextafter(a, inf); r < b; r = rfpl::nextafter(r, inf))
	{
		draw_counter.add_draw(r);
	}

	constexpr auto eps = std::numeric_limits<double>::epsilon();
	d_n = compute_kolmogorov_smirnov_statistic(draw_counter);

	CHECK(d_n <= eps);
}


TEST_CASE("ks-statistic:mixed-signs")
{
	using real_t = rfpl::binary8;

	auto f = rfpl::compute_kolmogorov_smirnov_statistic<real_t>;
	auto inf = std::numeric_limits<real_t>::infinity();
	auto a = rfpl::assemble_like(real_t{0}, 1, 2, 5);
	auto b = rfpl::assemble_like(real_t{0}, 0, 3, 1);
	auto draw_counter = rfpl::draw_counter<real_t>(a, b);

	auto d_n = f(draw_counter);
	CHECK(std::isnan(d_n));

	for(auto r = a; r < 0; r = rfpl::nextafter(r, inf))
	{
		auto n = rfpl::exponent(r) == 0 ? 1_z : 1_z << (rfpl::exponent(r) - 1_z);

		for(auto i = 0_z; i < n; ++i)
			draw_counter.add_draw(r);
	}

	d_n = f(draw_counter);

	CHECK(d_n == rfpl::compute_uniform_cdf(a, b, rfpl::signbit(b), rfpl::exponent(b)));

	draw_counter.add_draw(rfpl::assemble_like(a, 1, 0, 0));

	for(auto r = real_t{0}; r < b; r = rfpl::nextafter(r, inf))
	{
		auto n = rfpl::exponent(r) == 0 ? 1_z : 1_z << (rfpl::exponent(r) - 1_z);

		for(auto i = 0_z; i < n; ++i)
			draw_counter.add_draw(r);
	}

	d_n = f(draw_counter);

	CHECK(d_n == 0);
}



TEMPLATE_TEST_CASE("compute_ks_rejection_value", "", float, double)
{
	using real_t = TestType;

	auto f = rfpl::compute_ks_rejection_value<real_t>;
	auto approx = [] (real_t x) { return Approx(x).margin(1e-3).epsilon(0); };

	CHECK(f(1e-1, 8, 8) == approx(1.073 / 2));
	CHECK(f(1e-1, 8, 8) == approx(f(1e-1, 16, 16) * std::sqrt(2)));
	CHECK(f(1e-1, 20, 30) == approx(1.073 / std::sqrt(12)));

	CHECK(f(1e-3, 8, 8) == approx(1.858 / 2));

	CHECK(f(1, 123, 456) == 0);
}


TEMPLATE_TEST_CASE("compute_ks_rejection_value:errors", "", float, double)
{
	using real_t = TestType;

	auto f = rfpl::compute_ks_rejection_value<real_t>;

	CHECK(std::isnan(f(-0.1, 10, 11)));
	CHECK(std::isnan(f(1.1, 10, 12)));
	CHECK(std::isnan(f(0.1, 10, 0)));
	CHECK(std::isnan(f(0.1, 0, 14)));
}
