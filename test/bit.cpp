// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cstdint>
#include <rademacher-fpl/bit.hpp>

namespace rademacher_fpl_bit_log2p1_tests
{
	using rademacher_fpl::log2p1;

	static_assert(log2p1(std::uint8_t{0}) == 0u, "");
	static_assert(log2p1(std::uint8_t{1}) == 1u, "");
	static_assert(log2p1(std::uint8_t{2}) == 2u, "");
	static_assert(log2p1(std::uint8_t{3}) == 2u, "");
	static_assert(log2p1(std::uint8_t{4}) == 3u, "");
	static_assert(log2p1(std::uint8_t{5}) == 3u, "");
	static_assert(log2p1(std::uint8_t{6}) == 3u, "");
	static_assert(log2p1(std::uint8_t{7}) == 3u, "");
	static_assert(log2p1(std::uint8_t{8}) == 4u, "");
	static_assert(log2p1(std::uint8_t{255}) == 8u, "");

	static_assert(log2p1(0u) == 0u, "");
	static_assert(log2p1(1u) == 1u, "");
	static_assert(log2p1(2u) == 2u, "");
	static_assert(log2p1(3u) == 2u, "");
	static_assert(log2p1(4u) == 3u, "");
	static_assert(log2p1(5u) == 3u, "");
	static_assert(log2p1(6u) == 3u, "");
	static_assert(log2p1(7u) == 3u, "");
	static_assert(log2p1(8u) == 4u, "");
	static_assert(log2p1(65535u) == 16u, "");
	static_assert(log2p1(65536u) == 17u, "");
}
