// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cstdint>
#include <rademacher-fpl/traits.hpp>
#include <type_traits>


namespace traits = rademacher_fpl::traits;


namespace rademacher_fpl_traits_as_unsigned
{
	static_assert(
		sizeof(traits::as_unsigned<float>::type) == sizeof(float), ""
	);
	static_assert(
		not std::is_signed<traits::as_unsigned<float>::type>::value, ""
	);

	static_assert(
		sizeof(traits::as_unsigned<double>::type) == sizeof(double), ""
	);
	static_assert(
		not std::is_signed<traits::as_unsigned<double>::type>::value, ""
	);
}


namespace rademacher_fpl_traits_num_exponent_digits
{
	static_assert(traits::num_exponent_digits<float>::value == 8, "");
	static_assert(traits::num_exponent_digits<double>::value == 11, "");
}


namespace rademacher_fpl_traits_num_significand_digits
{
	static_assert(traits::num_significand_digits<float>::value == 23, "");
	static_assert(traits::num_significand_digits<double>::value == 52, "");
}


namespace rademacher_fpl_traits_max_exponent
{
	static_assert(traits::max_exponent<float>::value == 255, "");
	static_assert(traits::max_exponent<double>::value == 2047, "");
}


namespace rademacher_fpl_traits_max_significand
{
	using uint_t = std::uintmax_t;

	constexpr auto one = uint_t{1};
	constexpr auto max_sf = (one << 23) - one;
	constexpr auto max_sd = (one << 52) - one;

	static_assert(traits::max_significand<float>::value == max_sf, "");
	static_assert(traits::max_significand<double>::value == max_sd, "");
}
