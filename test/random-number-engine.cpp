// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <rademacher-fpl/random-number-engine.hpp>

namespace rfpl = rademacher_fpl;



TEST_CASE("xoshiro128+")
{
	SECTION("check 64-bit seed argument")
	{
		auto seed64 = (std::uint64_t{1} << 33) + 1u;
		auto seed32 = std::uint32_t(seed64);

		auto gen_s64 = rfpl::xoshiro128plus(seed64);
		auto gen_s32 = rfpl::xoshiro128plus(seed32);

		CHECK(gen_s64() != gen_s32());
	}

	SECTION("check non-zero state initialization")
	{
		auto gen = rfpl::xoshiro128plus(0u);

		CHECK(gen() != 0);
	}
}



TEST_CASE("add_with_carry")
{
	using Generator = rfpl::add_with_carry_engine<std::uint32_t, 32u, 3u, 17u>;

	SECTION("check 64-bit seed argument")
	{
		auto seed64 = (std::uint64_t{1} << 40) + 1u;
		auto seed32 = std::uint32_t(seed64);

		auto gen_s64 = Generator(seed64);
		auto gen_s32 = Generator(seed32);

		CHECK(gen_s64() != gen_s32());
	}

	SECTION("check non-zero state initialization")
	{
		auto gen = Generator(0u);

		CHECK(gen() != 0);
	}
}
