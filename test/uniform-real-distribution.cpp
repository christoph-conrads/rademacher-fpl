// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cassert>
#include <catch2/catch.hpp>
#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/generators.hpp>
#include <rademacher-fpl/statistics.hpp>
#include <rademacher-fpl/traits.hpp>
#include <rademacher-fpl/impl/uniform-int-distribution.hpp>
#include <rademacher-fpl/impl/uniform-real-distribution.hpp>
#include <vector>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;
using rfpl::binary8;


template<
	typename Real,
	typename Unsigned = rfpl::traits::as_fast_unsigned_t<Real>,
	class ExponentGenerator = rfpl::simple_generator<Unsigned>,
	class SignificandGenerator = rfpl::simple_generator<Unsigned>
>
void test_make_uniform_random_value(
	Real a, Real b,
	ExponentGenerator* p_gen_exponent,
	SignificandGenerator* p_gen_significand,
	std::size_t num_draws_per_bin = 2,
	double tolerance = std::numeric_limits<double>::epsilon()
)
{
	static_assert(std::is_floating_point<Real>::value, "");
	static_assert(not std::is_signed<Unsigned>::value, "");
	static_assert(
		sizeof(Unsigned) >= sizeof(rfpl::traits::as_unsigned_t<Real>), ""
	);

	REQUIRE(a < b);
	REQUIRE(num_draws_per_bin > 0);

	auto dc = rfpl::draw_counter<Real>(a, b);
	auto& gen_exponent = *p_gen_exponent;
	auto& gen_significand = *p_gen_significand;
	auto num_draws = num_draws_per_bin * compute_num_virtual_bins(a, b);

	REQUIRE(gen_exponent.min() == 0);
	REQUIRE(gen_significand.min() == 0);

	for(auto i = 0_z; i < num_draws; ++i)
	{
		auto c = rfpl::urd_impl::make_uniform_random_value<
			Real, ExponentGenerator, SignificandGenerator, Unsigned
		>(
			a, b, &gen_exponent, &gen_significand
		);

		REQUIRE(c >= a);
		REQUIRE(c < b);

		dc.add_draw(c);
	}

	auto d_n = rfpl::compute_kolmogorov_smirnov_statistic(dc);

	CHECK(d_n <= tolerance);
}


TEST_CASE("negative-interval")
{
	auto a = binary8(1, 2, 7);
	auto b = binary8(0, 0, 1);
	auto gen = std::mt19937_64();
	auto m = 2u;
	auto num_draws = m * rfpl::compute_num_virtual_bins(a, b);
	auto tol = rfpl::compute_ks_rejection_value(0.5, num_draws, num_draws);

	test_make_uniform_random_value(a, b, &gen, &gen, m, tol);
}



TEST_CASE("symmetric-interval")
{
	auto a = binary8(1, 2, 7);
	auto b = binary8(0, 3, 0);
	auto gen = std::mt19937_64();
	auto m = 2u;
	auto num_draws = m * rfpl::compute_num_virtual_bins(a, b);
	auto tol = rfpl::compute_ks_rejection_value(0.5, num_draws, num_draws);

	test_make_uniform_random_value(a, b, &gen, &gen, m, tol);
}



TEST_CASE("same-exponents:simple")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto a = binary8(binary8::BIAS, 1);
	auto b = binary8(binary8::BIAS, 6);
	auto gen = Generator();

	test_make_uniform_random_value(a, b, &gen, &gen);
}


TEST_CASE("same-exponent:nextafter")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto a = binary8(2, 4);
	auto b = binary8(3, 0);
	auto gen = Generator();

	test_make_uniform_random_value(a, b, &gen, &gen);
}



TEST_CASE("pow2:one-two")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto one = binary8(binary8::BIAS, 0);
	auto two = binary8(binary8::BIAS+1, 0);
	auto gen = Generator();

	test_make_uniform_random_value(one, two, &gen, &gen);
}



TEST_CASE("pow2:negative")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	auto a = binary8(1, binary8::BIAS, 7);
	auto b = binary8(1, binary8::BIAS-1, 7);
	auto gen = Generator();

	test_make_uniform_random_value(a, b, &gen, &gen);
}



TEST_CASE("pow2:non-negative")
{
	constexpr auto num_significand_digits =
		rfpl::traits::num_significand_digits<binary8>::value;
	constexpr auto num_significands = 1 << num_significand_digits;

	auto a = binary8(0);
	auto b = std::numeric_limits<binary8>::infinity();
	auto gen_exponent = rfpl::delayed_generator<std::uint32_t, num_significands-1>();
	auto gen_significand = rfpl::simple_generator<std::uint32_t>();

	test_make_uniform_random_value(a, b, &gen_exponent, &gen_significand);
}



TEST_CASE("pow2:non-negative:vbin-overflow")
{
	constexpr auto NUM_EXPONENT_DIGITS =
		rfpl::traits::num_exponent_digits<binary8>::value;
	constexpr auto MAX_FINITE_EXPONENT = (1_z << NUM_EXPONENT_DIGITS) - 2_z;

	using exponent_generator_type = rfpl::nested_generator<
		rfpl::simple_generator<std::uint8_t>,
		rfpl::simple_generator<std::uint8_t, 63>
	>;
	using significand_generator_type =
		rfpl::delayed_generator<std::uint16_t, (1_z<<MAX_FINITE_EXPONENT) - 1_z>;

	auto a = binary8(0);
	auto b = std::numeric_limits<binary8>::infinity();
	auto gen_exponent = exponent_generator_type();
	auto gen_significand = significand_generator_type();

	test_make_uniform_random_value<binary8, std::uint8_t>(
		a, b, &gen_exponent, &gen_significand
	);
}



TEST_CASE("a-pow2:denormal-a")
{
	auto a = binary8(0);
	auto b = binary8(1, 7);
	auto gen_exponent = rfpl::simple_generator<std::size_t>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();

	test_make_uniform_random_value(a, b, &gen_exponent, &gen_significand);
}


TEST_CASE("a-pow2:normal-a")
{
	auto a = binary8(1, 0);
	auto b = binary8(2, 7);
	auto gen_exponent = rfpl::simple_generator<std::size_t>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();

	test_make_uniform_random_value(a, b, &gen_exponent, &gen_significand);
}


TEST_CASE("a-pow2:normal-a:vbin-overflow")
{
	auto a = binary8(1, 0);
	auto b = binary8(8, 7);
	auto gen_exponent = rfpl::simple_generator<std::uint8_t>();
	auto gen_significand = rfpl::delayed_generator<std::uint8_t, 127>();

	test_make_uniform_random_value<decltype(a), std::uint8_t>(
		a, b, &gen_exponent, &gen_significand
	);
}



TEST_CASE("same-sign:normal-a")
{
	auto a = binary8(1, 3);
	auto b = binary8(3, 7);
	auto gen_exponent = rfpl::simple_generator<std::size_t>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();

	test_make_uniform_random_value(a, b, &gen_exponent, &gen_significand);
}


TEST_CASE("same-sign:normal-a:vbin-overflow")
{
	auto a = binary8(1, 3);
	auto b = binary8(8, 7);
	auto gen_exponent = rfpl::nested_generator<
		rfpl::simple_generator<std::uint8_t>,
		rfpl::simple_generator<std::uint8_t, 63>
	>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();
	auto m = 2u;
	auto num_draws = m * rfpl::compute_num_virtual_bins(a, b);
	auto tol = rfpl::compute_ks_rejection_value(0.1, num_draws, num_draws);

	test_make_uniform_random_value<binary8, std::uint8_t>(
		a, b, &gen_exponent, &gen_significand, m, tol
	);
}


TEST_CASE("same-sign:denormal-a")
{
	auto a = binary8(0, 3);
	auto b = binary8(2, 7);
	auto gen_exponent = rfpl::simple_generator<std::uint8_t>();
	auto gen_significand = rfpl::simple_generator<std::uint8_t>();

	test_make_uniform_random_value<binary8, std::uint8_t>(
		a, b, &gen_exponent, &gen_significand
	);
}



TEST_CASE("mixed-sign")
{
	auto a = binary8(1, 1, 3);
	auto b = binary8(0, 2, 1);
	auto gen_exponent = rfpl::simple_generator<std::size_t>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();

	test_make_uniform_random_value(
		a, b, &gen_exponent, &gen_significand
	);
}


TEST_CASE("mixed-sign:denormals")
{
	auto a = binary8(1, 0, 7);
	auto b = binary8(0, 0, 7);
	auto gen_exponent = rfpl::simple_generator<std::size_t>();
	auto gen_significand = rfpl::simple_generator<std::size_t>();

	test_make_uniform_random_value(
		a, b, &gen_exponent, &gen_significand
	);
}


TEST_CASE("mixed-sign:vbin-overflow")
{
	using Generator = rfpl::type_wrapper<std::uint8_t, std::mt19937>;

	SECTION("[a,b), abs(a) > abs(b)")
	{
		auto a = binary8(1, 4, 5);
		auto b = binary8(0, 2, 3);
		auto gen_exponent = Generator();
		auto gen_significand = Generator();
		auto m = 50u;
		auto num_draws = m * rfpl::compute_num_virtual_bins(a, b);
		auto tol = rfpl::compute_ks_rejection_value(0.5, num_draws, num_draws);

		test_make_uniform_random_value<binary8, std::uint8_t>(
			a, b, &gen_exponent, &gen_significand, m, tol
		);
	}

	SECTION("[a,b), abs(a) >> abs(b)")
	{
		auto a = binary8(1, 10, 5);
		auto b = binary8(0, 2, 3);
		auto gen_exponent = Generator();
		auto gen_significand = Generator();
		auto m = 2u;
		auto num_draws = m * rfpl::compute_num_virtual_bins(a, b);
		auto tol = rfpl::compute_ks_rejection_value(0.5, num_draws, num_draws);

		test_make_uniform_random_value<binary8, std::uint8_t>(
			a, b, &gen_exponent, &gen_significand, m, tol
		);
	}
}
