// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/generators.hpp>
#include <rademacher-fpl/impl/uniform-int-distribution.hpp>
#include <random>
#include <vector>


namespace rfpl = rademacher_fpl;
using namespace rfpl::literals;


namespace rademacher_fpl_has_pow2_range_tests
{
	using rademacher_fpl::has_pow2_range;

	static_assert(not has_pow2_range<std::minstd_rand>::value, "");
	static_assert(has_pow2_range<std::mt19937>::value, "");
	static_assert(has_pow2_range<std::mt19937_64>::value, "");
	static_assert(has_pow2_range<std::ranlux24>::value, "");
	static_assert(has_pow2_range<std::ranlux48>::value, "");
}


TEST_CASE("rejection-sampling")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;
	using Distribution = rfpl::uniform_int_distribution<unsigned>;

	constexpr auto a = 1u;
	constexpr auto b = 129u;
	auto dist = Distribution(a, b);


	SECTION("Test lower bound")
	{
		auto gen = Generator();
		CHECK(dist(gen) == 1);
		CHECK(gen.num_calls() == 1);
	}

	SECTION("Test upper bound")
	{
		auto gen = Generator(b-1);
		CHECK(dist(gen) == b);
		CHECK(gen.num_calls() == 1);
	}

	SECTION("Test generator rejection")
	{
		auto gen = Generator(b);
		CHECK(dist(gen) == 1);
		CHECK(gen.num_calls() == 128);
	}

	SECTION("Test generator rejection II")
	{
		auto gen = Generator(b+1);
		CHECK(dist(gen) == 1);
		CHECK(gen.num_calls() == 127);
	}


	SECTION("Test uniform distribution")
	{
		auto gen = Generator();
		auto num_bins = b - a + 1;
		auto bins = std::vector<std::size_t>(num_bins);
		auto num_draws = 2_z * num_bins;

		for(auto i = 0_z; i < num_draws; ++i)
		{
			auto k = dist(gen);

			REQUIRE(k >= a);
			REQUIRE(k <= b);

			++bins.at(k-a);
		}

		for(auto n : bins)
			CHECK(n == 2);

		auto generator_range = Generator::max() - Generator::min() + 1;
		auto expected_num_calls = num_draws + generator_range - num_bins;

		CHECK(gen.num_calls() == expected_num_calls);
	}
}



TEST_CASE("draw_uniform_int:uint8_t.uint8_t")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	constexpr auto max = std::size_t{Generator::max()};
	auto gen = Generator();
	auto num_draws = 2 * max;
	auto dist = [] (Generator& gen) {
		return rfpl::impl::draw_uniform_int(&gen, std::uint8_t{0});
	};

	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(dist(gen) == i % (max + 1));
	}

	CHECK(gen.num_calls() == num_draws);
}


TEST_CASE("draw_uniform_int:uint8_t.uint16_t")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	constexpr auto max = std::size_t{Generator::max()};
	constexpr auto a = std::uint16_t{0};
	constexpr auto b = std::uint16_t{Generator::max()};
	auto num_draws = 2 * max;
	auto gen = Generator();
	auto dist = [] (Generator& gen) {
		return rfpl::impl::draw_uniform_int(&gen, a, b);
	};

	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(dist(gen) == i % (max + 1));
	}

	CHECK(gen.num_calls() == num_draws);
}


TEST_CASE("draw_uniform_int:uint16_t.uint8_t")
{
	using Generator = rfpl::simple_generator<std::uint16_t>;

	auto gen = Generator();
	auto max = std::size_t{std::numeric_limits<std::uint8_t>::max()};
	auto num_draws = 2 * max;
	auto dist = [] (Generator& gen) {
		return rfpl::impl::draw_uniform_int(&gen, std::uint8_t{0});
	};

	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(dist(gen) == i % (max + 1));
	}

	CHECK(gen.num_calls() == num_draws);
}



TEST_CASE("draw_uniform_int:pow2-range")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	constexpr auto a = 32u;
	constexpr auto b = 63u;
	auto gen = Generator();
	auto num_draws = 2_z * std::size_t{Generator::max()};
	auto dist = [] (Generator& gen) {
		return rfpl::impl::draw_uniform_int(&gen, a, b);
	};

	for(auto i = 0_z; i < num_draws; ++i)
	{
		CHECK(gen.num_calls() == i);
		CHECK(dist(gen) == i % (b-a+1) + a);
	}

	CHECK(gen.num_calls() == num_draws);
}



TEST_CASE("draw_uniform_int:large-pow2-range")
{
	using Generator = rfpl::simple_generator<std::uint8_t>;

	constexpr auto a = 456u;
	constexpr auto b = 1479u;

	static_assert(b - a > Generator::max() - Generator::min(), "");

	auto gen = Generator();
	auto dist = [] (Generator& gen) {
		return rfpl::impl::draw_uniform_int(&gen, a, b);
	};

	CHECK(dist(gen) == a + 1u);
	CHECK(gen.num_calls() == 2u);

	CHECK(dist(gen) == a + 512u + 3u);
	CHECK(gen.num_calls() == 4u);

	CHECK(dist(gen) == a + 5u);
	CHECK(gen.num_calls() == 6u);
}
