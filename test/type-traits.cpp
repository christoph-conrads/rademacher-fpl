// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cstdint>
#include <rademacher-fpl/type-traits.hpp>
#include <type_traits>

namespace rademacher_fpl_type_traits_test
{
	using std::is_same;
	using std::uint8_t;
	using std::uint16_t;
	using std::uint32_t;
	template<typename T, typename U> using common_t =
		typename rademacher_fpl::common_type<T, U>::type
	;

	static_assert(is_same<common_t<uint8_t, uint8_t>, uint8_t>::value, "");

	static_assert(is_same<common_t<uint8_t,  uint16_t>, uint16_t>::value, "");
	static_assert(is_same<common_t<uint16_t, uint8_t>,  uint16_t>::value, "");
	static_assert(is_same<common_t<uint16_t, uint16_t>, uint16_t>::value, "");

	static_assert(is_same<common_t<uint8_t,  uint32_t>, uint32_t>::value, "");
	static_assert(is_same<common_t<uint16_t, uint32_t>, uint32_t>::value, "");
	static_assert(is_same<common_t<uint32_t, uint8_t>,  uint32_t>::value, "");
	static_assert(is_same<common_t<uint32_t, uint16_t>, uint32_t>::value, "");
	static_assert(is_same<common_t<uint32_t, uint32_t>, uint32_t>::value, "");
}
