// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <catch2/catch.hpp>
#include <cerrno>
#include <cfenv>
#include <features.h>
#include <limits>
#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/impl/math.hpp>


namespace rfpl = rademacher_fpl;


TEST_CASE("simple")
{
	auto u = 1.0f;

	CHECK(rfpl::signbit(u) == 0);
	CHECK(rfpl::significand(u) == 0);

	auto v = -1.0f;

	CHECK(rfpl::signbit(v) == 1);

	CHECK(rfpl::exponent(u) == rfpl::exponent(v));
	CHECK(rfpl::significand(v) == 0);
}



TEMPLATE_TEST_CASE("abs", "", rfpl::binary8, float, double)
{
	using real_t = TestType;

	auto u = real_t{-21};
	auto v = rfpl::impl::abs(u);

	CHECK(rfpl::signbit(u) == 1);
	CHECK(rfpl::signbit(v) == 0);
	CHECK(rfpl::exponent(u) == rfpl::exponent(v));
	CHECK(rfpl::significand(u) == rfpl::significand(v));

	CHECK(u == -v);
}



TEMPLATE_TEST_CASE("isfinite", "", rfpl::binary8, float, double)
{
	using real_t = TestType;

	auto f = rfpl::impl::isfinite<real_t>;

	CHECK(f(real_t{0}));
	CHECK(f(-real_t{0}));
	CHECK(f(real_t{+1}));
	CHECK(f(real_t{-1}));

	const auto denorm_min = std::numeric_limits<real_t>::denorm_min();

	CHECK(f(denorm_min));
	CHECK(f(-denorm_min));

	const auto max = std::numeric_limits<real_t>::max();

	CHECK(f(max));
	CHECK(f(-max));

	const auto inf = std::numeric_limits<real_t>::infinity();

	CHECK(not f(inf));
	CHECK(not f(-inf));

	const auto nan = std::numeric_limits<real_t>::quiet_NaN();

	CHECK(not f(nan));
	CHECK(not f(+nan));
}



TEMPLATE_TEST_CASE("isnan", "", float, double)
{
	using real_t = TestType;

	constexpr auto zero = real_t{0};
	constexpr auto one = real_t{1};
	constexpr auto inf = std::numeric_limits<real_t>::infinity();

	// use `std::isnan` as a sanity check and to detect deviations from the
	// documented behavior
	using function_t = bool(*)(real_t);
	function_t fs[] = {&rfpl::impl::isnan, &std::isnan};

	for(auto f : fs)
	{
		CHECK(not f(-one));
		CHECK(not f(-zero));
		CHECK(not f(zero));
		CHECK(not f(one));
		CHECK(not f(inf));

		CHECK(not f(-(inf/zero)));
		CHECK(not f(-(one/zero)));
		CHECK(not f(one/zero));
		CHECK(not f(inf/zero));

		CHECK(f(zero/zero));
		CHECK(f(-zero/zero));
		CHECK(f(inf/inf));
		CHECK(f(inf/-inf));
	}
}



TEMPLATE_TEST_CASE("nextafter:identity", "", rfpl::binary8, float, double)
{
	using real_t = TestType;

	auto x = GENERATE(
		real_t{0},
		std::numeric_limits<real_t>::denorm_min(),
		std::numeric_limits<real_t>::min(),
		real_t{1},
		std::numeric_limits<real_t>::max(),
		std::numeric_limits<real_t>::infinity(),
		std::numeric_limits<real_t>::quiet_NaN()
	);

	errno = 0;
	std::feclearexcept(FE_ALL_EXCEPT);

	auto y = rfpl::impl::nextafter(x, x);

	CHECK(errno == 0);
#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 23
	CHECK(std::fetestexcept(FE_ALL_EXCEPT) == 0);
#endif

	if(rfpl::isnan(x))
		CHECK(rfpl::isnan(y));
	else
		CHECK(x == y);
}


TEMPLATE_TEST_CASE("nextafter:simple", "", rfpl::binary8, float, double)
{
	using real_t = TestType;

	constexpr auto max_exponent = rfpl::traits::max_exponent<real_t>::value;

	const auto denorm_min = std::numeric_limits<real_t>::denorm_min();
	const auto inf = std::numeric_limits<real_t>::infinity();
	const auto nan = std::numeric_limits<real_t>::quiet_NaN();

	auto f = rfpl::impl::nextafter<real_t>;

	// check zero signs
	auto z_0 = f(+denorm_min, -inf);

	CHECK(z_0 == 0);
	CHECK(not rfpl::signbit(z_0));
	CHECK(f(z_0, -inf) == -denorm_min);

	auto z_1 = f(-denorm_min, +inf);

	CHECK(z_1 == 0);
	CHECK(rfpl::signbit(z_1));
	CHECK(f(z_1, +inf) == denorm_min);

	auto x = GENERATE(
		real_t{0},
		std::numeric_limits<real_t>::denorm_min(),
		std::numeric_limits<real_t>::min(),
		real_t{1},
		std::numeric_limits<real_t>::max(),
		std::numeric_limits<real_t>::infinity()
	);
	auto d = GENERATE(
		real_t{0},
		std::numeric_limits<real_t>::denorm_min(),
		std::numeric_limits<real_t>::min(),
		real_t{1},
		std::numeric_limits<real_t>::max(),
		std::numeric_limits<real_t>::infinity()
	);

	CHECK(rfpl::isnan(f(x, nan)));
	CHECK(rfpl::isnan(f(nan, x)));
	CHECK(rfpl::isnan(f(d, nan)));
	CHECK(rfpl::isnan(f(nan, d)));

	errno = 0;
	std::feclearexcept(FE_ALL_EXCEPT);

	auto y = f(x, d);
	auto except =
		x == y                            ? 0 :
		rfpl::exponent(y) == 0            ? FE_INEXACT | FE_UNDERFLOW :
		rfpl::exponent(y) == max_exponent ? FE_INEXACT | FE_OVERFLOW : 0
	;
	auto errno_value =
		(x == 0) or (except == 0) or ((math_errhandling & MATH_ERRNO) == 0)
		? 0
		: ERANGE
	;

	CHECK(errno == errno_value);
	CHECK(std::fetestexcept(except) == except);

	if(x < d)
		CHECK(x < y);
	if(x == d)
		CHECK(x == y);
	if(x > d)
		CHECK(x > y);
}


TEMPLATE_TEST_CASE("nextafter:compare-to-std", "", float, double)
{
	using real_t = TestType;

	auto x = GENERATE(
		real_t{0},
		std::numeric_limits<real_t>::denorm_min(),
		std::numeric_limits<real_t>::min(),
		real_t{1},
		std::numeric_limits<real_t>::max(),
		std::numeric_limits<real_t>::infinity(),
		std::numeric_limits<real_t>::quiet_NaN()
	);
	auto d = GENERATE(
		real_t{0},
		std::numeric_limits<real_t>::denorm_min(),
		std::numeric_limits<real_t>::min(),
		real_t{1},
		std::numeric_limits<real_t>::max(),
		std::numeric_limits<real_t>::infinity(),
		std::numeric_limits<real_t>::quiet_NaN()
	);

	errno = 0;
	std::feclearexcept(FE_ALL_EXCEPT);

	auto y = rfpl::impl::nextafter(x, d);
#if __GLIBC__ == 2 && __GLIBC_MINOR__ >= 23
	auto error = errno;
#endif
	auto exceptions = std::fetestexcept(FE_INEXACT|FE_OVERFLOW|FE_UNDERFLOW);

	errno = 0;
	std::feclearexcept(FE_ALL_EXCEPT);

	auto z = std::nextafter(x, d);

#if __GLIBC__ == 2 && __GLIBC_MINOR__ >= 23
	REQUIRE(errno == error);
#endif
	REQUIRE(std::fetestexcept(exceptions) == exceptions);

	if(rfpl::isnan(y))
	{
		REQUIRE(rfpl::isnan(z));
	}
	else
	{
		REQUIRE(rfpl::signbit(y) == rfpl::signbit(z));
		REQUIRE(y == z);
	}
}



TEMPLATE_TEST_CASE("nextafter_0", "", rfpl::binary8, float, double)
{
	using real_t = TestType;

	auto f = rfpl::nextafter_0<real_t>;
	auto check_for = [f] (int except, real_t x, real_t d) {
		errno = 0;
		std::feclearexcept(FE_ALL_EXCEPT);

		auto y = f(x, d);

		CHECK(errno == (x != 0 ? ERANGE : 0));
		CHECK(std::fetestexcept(except) == except);

		auto z = x == 0 ? y : rfpl::nextafter(x, d);

		CHECK(y == z);
		CHECK(rfpl::signbit(y) == rfpl::signbit(z));

		return y;
	};
	auto check_identity = [f] (real_t x) {
		errno = 0;
		std::feclearexcept(FE_ALL_EXCEPT);

		auto y = f(x, x);

		CHECK(errno == 0);
#if __GLIBC__ >= 2 && __GLIBC__MINOR__ >= 23
		CHECK(std::fetestexcept(FE_ALL_EXCEPT) == 0);
#endif

		if(rfpl::isnan(x))
			CHECK(rfpl::isnan(y));
		else
			CHECK(x == y);
	};

	const auto zero = real_t{0};
	const auto denorm_min = std::numeric_limits<real_t>::denorm_min();
	const auto one = real_t{1};
	const auto nan = std::numeric_limits<real_t>::quiet_NaN();

	check_identity(-denorm_min);
	check_identity(-zero);
	check_identity(zero);
	check_identity(denorm_min);
	check_identity(one);
	check_identity(nan);

	CHECK(check_for(FE_UNDERFLOW, -zero, -one) == -denorm_min);
	CHECK(check_for(0, -zero, -zero) == -zero);
	CHECK(check_for(0, -zero, zero) == zero);
	CHECK(check_for(0, -zero, one) == zero);
	CHECK(rfpl::isnan(f(-zero, nan)));

	CHECK(check_for(0, zero, -one) == -zero);
	CHECK(check_for(0, zero, -zero) == -zero);
	CHECK(check_for(0, zero, zero) == zero);
	CHECK(check_for(FE_UNDERFLOW, zero, one) == denorm_min);
	CHECK(rfpl::isnan(f(zero, nan)));
}



TEMPLATE_TEST_CASE("count_num_values_in", "", float, double)
{
	using Real = TestType;
	using Integer = typename rfpl::traits::as_unsigned<Real>::type;

	constexpr auto num_significand_digits =
		rfpl::traits::num_significand_digits<Real>::value;
	constexpr auto num_significands = Integer{1} << num_significand_digits;
	constexpr auto n_s = num_significands;

	auto f = rfpl::count_num_values_in<Real>;

	CHECK(f(Real{1}, Real{2}) == n_s);
	CHECK(f(Real{1}, Real{3}) == n_s + n_s/2);
	CHECK(f(Real{1}, Real{4}) == 2 * n_s);

	// reminder: 2^{min_exponent - 1} is the normal number smallest in modulus
	constexpr auto min_exponent = std::numeric_limits<Real>::min_exponent;
	constexpr auto max_exponent = std::numeric_limits<Real>::max_exponent;
	constexpr auto num_exponents = Integer{max_exponent - min_exponent + 2};
	constexpr auto inf = std::numeric_limits<Real>::infinity();

	CHECK(f(Real{0}, inf) == num_exponents * num_significands);
	CHECK(f(-inf, +inf) == 2 * f(Real{0}, inf));
}
