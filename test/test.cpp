// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/test.hpp>
#include <rademacher-fpl/impl/math.hpp>
#include <rademacher-fpl/impl/statistics.hpp>


using namespace rademacher_fpl::literals;


namespace rademacher_fpl {

template binary8 abs(binary8);
template bool isfinite(binary8);
template bool isnan(binary8);
template binary8 nextafter(binary8, binary8);
template binary8 nextafter_0(binary8, binary8);
template bool signbit(binary8);
template traits::as_unsigned_t<binary8> exponent(binary8 f);
template traits::as_unsigned_t<binary8> significand(binary8 f);
template struct assembler<binary8>;


template
typename traits::as_unsigned<binary8>::type count_num_values_in(binary8,binary8);

template struct draw_counter<binary8>;

template std::uintmax_t compute_num_virtual_bins<binary8>(binary8, binary8);

template
double compute_kolmogorov_smirnov_statistic(const draw_counter<binary8>&);

}
