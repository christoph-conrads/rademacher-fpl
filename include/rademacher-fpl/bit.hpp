// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_BIT_HPP
#define RADEMACHER_FPL_BIT_HPP

#include <limits>
#include <type_traits>


namespace rademacher_fpl {

/**
 * This function computes the number of bits needed to represent the integer `x`.
 *
 * This function will be added in C++20 to the standard library.
 *
 * @return `floor(log2(x)) + 1` for positive `x`, zero otherwise.
 */
template<typename T>
constexpr T log2p1(T x)
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	return
		x == 0 ? T{0} :
		x == 1 ? T{1} :
		true   ? log2p1(x/2u) + 1u : 0;
}

#if __GNUC__

// The explicit casts of `std::numeric_limits<T>::digits` to an unsigned type
// below prevent GCC 7.3.0 from generating an unnecessary `cltq` instruction on
// x86-64 (`cltq` sign-extends the register `eax` into `rax`).

constexpr unsigned char log2p1(unsigned char n) { return log2p1(unsigned{n}); }
constexpr unsigned short log2p1(unsigned short n) {return log2p1(unsigned{n});}

constexpr unsigned log2p1(unsigned n)
{
	using Limits = std::numeric_limits<unsigned>;

	return n == 0u
		? 0u
		: unsigned(Limits::digits) - __builtin_clz(n)
	;
}

constexpr unsigned long log2p1(unsigned long n)
{
	using Limits = std::numeric_limits<unsigned long>;

	return n == 0u
		? 0u
		: unsigned(Limits::digits) - __builtin_clzl(n)
	;
}

constexpr unsigned long long log2p1(unsigned long long n)
{
	using Limits = std::numeric_limits<unsigned long long>;

	return n == 0u
		? 0u
		: unsigned(Limits::digits) - __builtin_clzll(n)
	;
}
#endif


/**
 * @return true if `n = 2^k-1` for some natural number `k`, false otherwise.
 */
template<typename T>
constexpr bool ispow2m1(T n)
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	return (n & T(n+1u)) == 0;
}

}

#endif
