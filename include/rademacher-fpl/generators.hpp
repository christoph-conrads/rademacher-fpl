// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_GENERATORS_HPP
#define RADEMACHER_FPL_GENERATORS_HPP

#include <cstdint>
#include <limits>
#include <rademacher-fpl/bit.hpp>
#include <type_traits>


namespace rademacher_fpl {

template<typename T, T Max = std::numeric_limits<T>::max()>
struct simple_generator
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	using result_type = T;

	static constexpr result_type min() { return std::numeric_limits<T>::min(); }
	static constexpr result_type max() { return Max; }
	static constexpr std::uintmax_t period_length()
	{ return std::uintmax_t{Max} + 1; }


	simple_generator() {}
	explicit simple_generator(T state) : state_(state) {}

	std::uintmax_t num_calls() const { return num_calls_; }


	result_type operator() ()
	{
		auto ret = state_;
		state_ = Max + 1 == 0 ? state_ + 1 : (state_ + 1) % (Max + 1);
		num_calls_ = num_calls_ + 1;

		return ret;
	}


	T state_ = 0;
	std::uintmax_t num_calls_ = 0;
};



template<class OuterGenerator, class InnerGenerator>
struct nested_generator
{
	static_assert(
		std::is_same<
			typename OuterGenerator::result_type,
			typename InnerGenerator::result_type
		>::value,
		""
	);

	using result_type = typename OuterGenerator::result_type;


	static constexpr result_type min()
	{
		return OuterGenerator::min() <= InnerGenerator::min()
			? OuterGenerator::min()
			: InnerGenerator::min()
		;
	};

	static constexpr result_type max()
	{
		return OuterGenerator::max() >= InnerGenerator::max()
			? OuterGenerator::max()
			: InnerGenerator::max()
		;
	}


	result_type operator() ()
	{
		auto ret = use_inner_
			? inner_generator_()
			: outer_generator_()
		;

		use_inner_ = not use_inner_ and ret == 0;

		return ret;
	}


	const OuterGenerator& outer() const { return outer_generator_; }
	const InnerGenerator& inner() const { return inner_generator_; }


	bool use_inner_ = false;
	OuterGenerator outer_generator_;
	InnerGenerator inner_generator_;
};



template<typename T, T Delay, T Max = std::numeric_limits<T>::max()>
struct delayed_generator
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	using result_type = T;

	static constexpr result_type min() { return std::numeric_limits<T>::min(); }
	static constexpr result_type max() { return Max; }
	static constexpr std::uintmax_t period_length()	{
		return std::uintmax_t{Max} + 1;
	}


	delayed_generator() {}

	std::uintmax_t num_calls() const { return num_calls_; }


	result_type operator() ()
	{
		auto ret = state_;
		state_ =
			time_ < Delay ? state_ :
			 Max + 1 == 0 ? state_ + 1 : (state_ + 1) % (Max + 1)
		;
		time_ = time_ < Delay ? time_+1 : 0;
		num_calls_ = num_calls_ + 1;

		return ret;
	}


	T state_ = 0;
	T time_ = 0;
	std::uintmax_t num_calls_ = 0;
};


/**
 * This wrapper forces the result type to be smaller than it actually is.
 */
template<typename T, typename Generator>
struct type_wrapper
{
	static_assert(not std::is_signed<T>::value, "");
	static_assert(sizeof(T) < sizeof(typename Generator::result_type), "");
	// ensure the generator yields variates in `[0,2^n)` or this wrapper will
	// introduce bias
	static_assert(ispow2m1(Generator::min()), "");
	static_assert(ispow2m1(Generator::max()), "");

	using result_type = T;

	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }

	T operator() () { return generator_(); }


	Generator generator_;
};

}

#endif
