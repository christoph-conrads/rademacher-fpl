// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_TEST_HPP
#define RADEMACHER_FPL_TEST_HPP

#include <catch2/catch.hpp>
#include <rademacher-fpl/binary8.hpp>


namespace Catch
{

template<>
struct StringMaker<rademacher_fpl::binary8>
{
	static std::string convert(const rademacher_fpl::binary8& b) {
		return rademacher_fpl::to_string(b);
	}
};

}

#endif
