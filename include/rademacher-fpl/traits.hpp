// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_TRAITS_HPP
#define RADEMACHER_FPL_TRAITS_HPP

#include <climits>
#include <cstdint>
#include <limits>
#include <rademacher-fpl/literals.hpp>
#include <type_traits>


namespace rademacher_fpl {
namespace traits {
namespace impl {
	/**
	 * @return The smallest integer `k` such that `2^k >= n`.
	 */
	constexpr std::size_t next_power_of_two(std::size_t n, std::size_t exp=0)
	{
		return std::size_t{1} << exp >= n ? exp : next_power_of_two(n, exp+1u);
	}
}


	template<typename T>
	struct as_unsigned
	{
		using type = typename std::conditional<
			sizeof(T) == 1, std::uint8_t, typename std::conditional<
			sizeof(T) == 2, std::uint16_t, typename std::conditional<
			sizeof(T) == 4, std::uint32_t, typename std::conditional<
			sizeof(T) == 8, std::uint64_t, void
			>::type
			>::type
			>::type
		>::type;
	};

	template<typename T>
	using as_unsigned_t = typename as_unsigned<T>::type;


	template<typename T>
	struct as_fast_unsigned
	{
		using type = typename std::conditional<
			sizeof(T) <= sizeof(std::size_t),        std::size_t,        typename std::conditional<
			sizeof(T) <= sizeof(std::uint_fast32_t), std::uint_fast32_t, typename std::conditional<
			sizeof(T) <= sizeof(std::uint_fast64_t), std::uint_fast64_t, typename std::conditional<
			sizeof(T) <= sizeof(std::uintmax_t),     std::uintmax_t,
			void
			>::type
			>::type
			>::type
		>::type;
	};

	template<typename T>
	using as_fast_unsigned_t = typename as_fast_unsigned<T>::type;



	template<typename T>
	struct num_exponent_digits
	{
		using uint_t = typename as_unsigned<T>::type;

		static constexpr uint_t value = impl::next_power_of_two(
			std::numeric_limits<T>::max_exponent
			- std::numeric_limits<T>::min_exponent
			+ uint_t{2}
		);
	};


	template<typename T>
	struct num_significand_digits
	{
		using uint_t = typename as_unsigned<T>::type;

		static constexpr uint_t value =
			sizeof(T) * CHAR_BIT - 1u - num_exponent_digits<T>::value
		;
	};



	template<typename T>
	struct max_exponent
	{
		using uint_t = typename as_unsigned<T>::type;

		static_assert(
			sizeof(uint_t) * CHAR_BIT > num_exponent_digits<T>::value, ""
		);

		static constexpr uint_t one_ = uint_t{1};
		static constexpr uint_t value =
			(one_ << num_exponent_digits<T>::value) - one_
		;
	};


	template<typename T>
	struct max_significand
	{
		using uint_t = typename as_unsigned<T>::type;

		static_assert(
			sizeof(uint_t) * CHAR_BIT > num_significand_digits<T>::value, ""
		);

		static constexpr uint_t one_ = uint_t{1};
		static constexpr uint_t value =
			(one_ << num_significand_digits<T>::value) - one_
		;
	};
}
}

#endif
