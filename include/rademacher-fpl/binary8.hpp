// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_BINARY8_HPP
#define RADEMACHER_FPL_BINARY8_HPP

#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <limits>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/traits.hpp>
#include <string>
#include <vector>


namespace rademacher_fpl {

/**
 * An incomplete binary8 implementation for development purposes.
 */
struct binary8
{
	enum : std::uint8_t { BIAS = 7 };

	binary8(bool sign_p, std::uint8_t exponent, std::uint8_t fraction) :
		fraction_{fraction},
		exponent_{exponent},
		sign_{sign_p ? std::uint8_t{1} : std::uint8_t{0}}
	{
		assert(exponent_ == exponent);
		assert(fraction_ == fraction);
	}

	binary8(std::uint8_t exponent, std::uint8_t fraction) :
		binary8(false, exponent, fraction)
	{}

	template<typename Integer>
	binary8(Integer integer) :
		fraction_{0},
		exponent_{integer == 0 ? std::uint8_t(0) : std::uint8_t{BIAS}},
		sign_{integer < 0 ? std::uint8_t{1} : std::uint8_t{0}}
	{
		if(integer == 0)
			return;

		// this breaks if i == std::numeric_limits<Integer>::min()
		integer = std::abs(integer);

		assert(integer > 0);

		for(auto i = integer; i > 1; i /= 2)
		{
			++exponent_;

			if(exponent_ == 15)
				return;
		}

		auto num_digits = 3;
		auto ones = (1 << num_digits) - 1;
		auto signed_exponent = exponent_ - BIAS;
		fraction_ = signed_exponent < 3
			? (integer << (3 - signed_exponent)) & ones
			: (integer >> (signed_exponent - 3)) & ones;
	}


	std::uint8_t fraction_ : 3;
	std::uint8_t exponent_ : 4;
	std::uint8_t sign_ : 1;
};


static_assert(sizeof(binary8) == 1, "");


bool operator!= (binary8, binary8);
bool operator== (binary8, binary8);
bool operator< (binary8, binary8);


inline bool operator<= (binary8 l, binary8 r) {
	return not isnan(l) and not isnan(r) and (l == r or l < r);
}

inline bool operator> (binary8 l, binary8 r) {
	return r < l;
}

inline bool operator>= (binary8 l, binary8 r) {
	return r <= l;
}


inline binary8 operator+ (binary8 f) {
	return f;
}

inline binary8 operator- (binary8 f) {
	auto s = f.sign_ == 0;
	auto g = binary8(s, f.exponent_, f.fraction_);

	return g;
}


binary8 assemble_like(binary8, bool s, std::uint8_t e, std::uint8_t f);


std::string to_string(binary8);
}


namespace std
{

template<>
struct numeric_limits<rademacher_fpl::binary8>
{
	static constexpr bool is_specialized = true;
	static constexpr bool is_signed = true;
	static constexpr bool is_integer = false;
	static constexpr bool is_exact = false;
	static constexpr bool has_infinity = true;
	static constexpr bool has_quiet_NaN = true;
	static constexpr bool has_signaling_NaN = false;
	static constexpr int digits = 4;
	static constexpr int radix = 2;
	static constexpr int min_exponent = -5;
	static constexpr int max_exponent = 8;

	static rademacher_fpl::binary8 denorm_min() {
		return rademacher_fpl::binary8(0, 1);
	}

	static rademacher_fpl::binary8 min() {
		return rademacher_fpl::binary8(1, 0);
	}

	static rademacher_fpl::binary8 max() {
		return rademacher_fpl::binary8(14, 7);
	}

	static rademacher_fpl::binary8 infinity() {
		return rademacher_fpl::binary8(15, 0);
	}

	static rademacher_fpl::binary8 quiet_NaN() {
		return rademacher_fpl::binary8(15, 1);
	}
};


template<>
struct is_arithmetic<rademacher_fpl::binary8>
{
	static constexpr bool value = true;
};

template<>
struct is_floating_point<rademacher_fpl::binary8>
{
	static constexpr bool value = true;
};

template<>
struct is_scalar<rademacher_fpl::binary8>
{
	static constexpr bool value = true;
};

}

#endif
