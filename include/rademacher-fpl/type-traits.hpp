// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_TYPE_TRAITS_HPP
#define RADEMACHER_FPL_TYPE_TRAITS_HPP

#include <limits>
#include <type_traits>

namespace rademacher_fpl
{

/*
 * For testing purposes, this library sometimes uses small unsigned integer
 * types but due to certain wording in the C++ standard, operations involving
 * small integer types will be promoted to `int`. This behavior is reflected by
 * `std::common_type` which is why we provide our own implementation here.
 */
template<typename T, typename U>
struct common_type
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	static_assert(std::is_integral<U>::value, "");
	static_assert(not std::is_signed<U>::value, "");

	using type = typename std::conditional<
		std::numeric_limits<T>::digits >= std::numeric_limits<U>::digits, T, U
	>::type;
};

template<typename T, typename U>
using common_t = typename common_type<T, U>::type;

}

#endif
