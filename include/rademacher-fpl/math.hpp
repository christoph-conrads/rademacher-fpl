// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_MATH_HPP
#define RADEMACHER_FPL_MATH_HPP

#include <cassert>
#include <climits>
#include <cmath>
#include <cstring>
#include <rademacher-fpl/config.hpp>
#include <rademacher-fpl/traits.hpp>
#include <type_traits>
#include <utility>


namespace rademacher_fpl {
namespace impl {
}



// math.h clones allowing Koenig lookup for custom float types

template<typename Real>
Real abs(Real f);

inline float abs(float f) { return std::abs(f); }
inline double abs(double f) { return std::abs(f); }
inline long double abs(long double f) { return std::abs(f); }


template<typename Real>
bool isfinite(Real f);

inline bool isfinite(float f) { return std::isfinite(f); }
inline bool isfinite(double f) { return std::isfinite(f); }
inline bool isfinite(long double f) { return std::isfinite(f); }


template<typename Real>
bool isnan(Real f);

inline bool isnan(float f) { return std::isnan(f); }
inline bool isnan(double f) { return std::isnan(f); }
inline bool isnan(long double f) { return std::isnan(f); }


template<typename Real>
Real nextafter(Real x, Real d);

inline float nextafter(float x, float y) { return std::nextafter(x, y); }
inline double nextafter(double x, double y) { return std::nextafter(x, y); }
inline long double nextafter(long double x, long double y) {
	return std::nextafter(x, y);
}


/**
 * This function behaves like `nextafter` except that it treats signed zero and
 * unsigned zero as different numbers.
 */
template<typename Real>
Real nextafter_0(Real x, Real d);


template<typename Real>
typename std::enable_if<std::is_floating_point<Real>::value, bool>::type
signbit(Real f);

inline bool signbit(float f) { return std::signbit(f); }
inline bool signbit(double f) { return std::signbit(f); }
inline bool signbit(long double f) { return std::signbit(f); }
template<typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type signbit(T t) {
	return std::signbit(t);
}


// other functions

template<typename Real>
traits::as_unsigned_t<Real> exponent(Real f);

template<typename Real>
traits::as_unsigned_t<Real> significand(Real f);



template<typename Real>
struct assembler
{
	static Real execute(
		bool sign_p,
		typename traits::as_unsigned_t<Real> exponent,
		typename traits::as_unsigned_t<Real> significand
	);
};


template<typename Real, typename T1, typename T2>
Real assemble_like(Real, bool sign_p, T1&& exponent, T2&& significand)
{
	return assembler<Real>::execute(
		sign_p, std::forward<T1>(exponent), std::forward<T2>(significand)
	);
}

template<typename Real, typename T1, typename T2>
Real assemble_like(Real, T1&& exponent, T2&& significand)
{
	return assembler<Real>::execute(
		false, std::forward<T1>(exponent), std::forward<T2>(significand)
	);
}


/**
 * @return The number of floating-point values in the interval `[a, b)`.
 */
template<typename Real>
typename traits::as_unsigned<Real>::type count_num_values_in(Real a, Real b);

}

#endif
