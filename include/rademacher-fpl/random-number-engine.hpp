// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_RANDOM_NUMBER_ENGINE_HPP
#define RADEMACHER_FPL_RANDOM_NUMBER_ENGINE_HPP

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <type_traits>


namespace rademacher_fpl {
namespace impl_random_number_engine
{
	constexpr std::uint32_t rotl(std::uint32_t x, unsigned k)
	{
		return (x << k) | (x >> (32 - k));
	}
}

/**
 * This class implements a xoshiro128+ pseudo-random number generator.
 *
 * D. Blackman, S. Vigna: "Scrambled Linear Pseudorandom Number Generators."
 * 2018. arXiv:1805.01407v1
 *
 * See Table 4 of the paper for TestU01 BigCrush results (column "S" shows
 * "systematic" failures and column "R" contains "repeated" failures"). The
 * public domain code for xoshiro128plus by Blackman and Vigna can be downloaded
 * from the xoshiro/xoroshiro website:
 *
 *   http://xoshiro.di.unimi.it
 */
struct xoshiro128plus
{
	using result_type = std::uint32_t;

	static constexpr result_type max() {
		return std::numeric_limits<result_type>::max();
	}
	static constexpr result_type min() {
		return std::numeric_limits<result_type>::min();
	}


	explicit xoshiro128plus(std::uint64_t seed) :
		// initialize state with some ones here to avoid LFSR zeroland
		state_{
			0, ~std::uint32_t{0}, std::uint32_t(seed>>32), std::uint32_t(seed)
		}
	{
		// escape from zeroland, cf. Section 9 in Blackman/Vigna (2018)
		// this generator is fast so generously discard a few values
		discard(128);
	}


	std::uint32_t operator() ()
	{
		using impl_random_number_engine::rotl;

		auto retval = state_[0] + state_[3];
		auto t = state_[1] << 9;

		state_[2] ^= state_[0];
		state_[3] ^= state_[1];
		state_[1] ^= state_[2];
		state_[0] ^= state_[3];
		state_[2] ^= t;
		state_[3] = rotl(state_[3], 11);

		return retval;
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::uint32_t state_[4];
};


/**
 * This class implements an add-with-carry (AWC) pseudo-random number generator.
 *
 * This implementation supports only the word size 32 with 32-bit unsigned
 * integer types. This generator is initialized with the aid of a xoshiro128+
 * PRNG to avoid correlated outputs for AWC PRNGs with similar seeds.
 *
 * References:
 * * G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The
 *   Annals of Applied Probability, Vol. 1, No. 3, pp. 462--469. 1991.
 * * M. Matsumoto et al.: "Defects in Initialization of Pseudorandom Number
 *   Generators." In: ACM Transactions on Modeling and Computer Simulation,
 *   Vol. 17, No. 4, Article 15. 2007. DOI: 10.1145/1276927.1276928
 */
template<typename T, std::size_t w, std::size_t s, std::size_t r>
struct add_with_carry_engine
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");
	static_assert(w <= std::numeric_limits<T>::digits, "");
	static_assert(s > 0u, "");
	static_assert(r > s, "");

	// prototype implementation
	static_assert(w == std::numeric_limits<T>::digits, "");
	static_assert(w == 16u or w == 32u, "");

	using result_type = T;

	static constexpr auto long_lag = r;
	static constexpr auto short_lag = s;
	static constexpr auto word_size = w;
	// `std::subtract_with_carry_engine::default_seed` is 19780503. I have no
	// idea where this value is coming from so I use its larger prime factor
	// instead.
	static constexpr auto default_seed = std::uint32_t{387853};


	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }


	explicit add_with_carry_engine(std::uint64_t seed=default_seed)
	{
		auto gen = xoshiro128plus(seed);

		for(auto& x : xs_)
			x = gen();

		// this condition prevents entering the two unescapable states
		carry_ = xs_[0] == 0 ? 1u : 0u;

		// ensure entering a periodic sequence
		discard(r);
	}


	T operator() ()
	{
		using UInt = typename std::conditional<
			w == 16u, std::uint32_t, typename std::conditional<
			w == 32u, std::uint64_t, void
		>::type>::type;

		auto i = index_;
		auto j = index_ >= s ? index_ - s : index_ + r - s;
		auto u = xs_[i];
		auto v = xs_[j];

		assert(carry_ == 0 or carry_ == 1);

		auto x = UInt{u} + v + carry_;
		auto c = x >> w;

		xs_[i] = T(x);
		carry_ = c;
		index_ = index_ + 1u == r ? 0u : index_ + 1u;

		return x;
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::size_t index_ = 0;
	T xs_[r] = { 0 };
	std::uint8_t carry_ = 0;
};



// RANLUX subtract-with-borrow(2^w, s, r)
using ranlux16_base =
	std::subtract_with_carry_engine<std::uint16_t, 16u, 3u, 11u>
;
using ranlux32_base =
	std::subtract_with_carry_engine<std::uint32_t, 32u, 3u, 17u>
;
using ranlux64_base =
	std::subtract_with_carry_engine<std::uint64_t, 64u, 4u, 26u>
;

using ranlux16 = std::discard_block_engine<ranlux16_base, 127u, 11u>;
using ranlux32 = std::discard_block_engine<ranlux32_base, 293u, 17u>;
using ranlux64 = std::discard_block_engine<ranlux64_base, 787u, 26u>;

using fast_ranlux16 = std::discard_block_engine<ranlux16_base, 37u, 11u>;
using fast_ranlux32 = std::discard_block_engine<ranlux32_base, 73u, 17u>;
using fast_ranlux64 = std::discard_block_engine<ranlux64_base, 197u, 26u>;


// RANLUX add-with-carry(2^w, r, s)
using ranlux16_awc_base = add_with_carry_engine<std::uint16_t, 16u, 2u, 9u>;
using ranlux32_awc_base = add_with_carry_engine<std::uint32_t, 32u, 3u, 16u>;

using ranlux16_awc = std::discard_block_engine<ranlux16_awc_base, 97u, 9u>;
using ranlux32_awc = std::discard_block_engine<ranlux32_awc_base, 277u, 16u>;

using fast_ranlux16_awc = std::discard_block_engine<ranlux16_awc_base, 23, 9u>;
using fast_ranlux32_awc = std::discard_block_engine<ranlux32_awc_base, 71u, 16u>;


/**
 * Use this engine if you have no clue about pseudo-random number generators.
 */
using default_engine = ranlux32_awc;

}

#endif
