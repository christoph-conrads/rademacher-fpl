// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_SPAN_HPP
#define RADEMACHER_FPL_SPAN_HPP

#include <cassert>
#include <cstddef>
#include <iterator>
#include <limits>
#include <type_traits>

namespace rademacher_fpl {
namespace impl {
	enum : std::size_t {
		dynamic_extent = std::numeric_limits<std::size_t>::max()
	};

	template<typename T, std::size_t Extent = dynamic_extent>
	struct span_base
	{
		static constexpr std::size_t count_ = Extent;

		constexpr span_base() noexcept = default;
		constexpr span_base(const span_base&) noexcept = default;

		explicit span_base(std::size_t count)
		{
			assert(count == count_);
			(void) count;
		}
	};

	template<typename T>
	struct span_base<T, dynamic_extent>
	{
		constexpr span_base() noexcept = default;
		constexpr span_base(const span_base&) noexcept = default;

		explicit span_base(std::size_t count) :
			count_(count)
		{}

		std::size_t count_ = std::numeric_limits<std::size_t>::max();
	};
}


template<typename T, std::size_t Extent = impl::dynamic_extent>
struct span : private impl::span_base<T, Extent>
{
	using element_type = T;
	using value_type = typename std::remove_cv<T>::type;
	using index_type = std::size_t;
	using difference_type = typename std::make_signed<index_type>::type;
	using pointer = T*;
	using reference = T&;
	using iterator = T*;
	using const_iterator = const T*;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using const_reverse_iterator = std::reverse_iterator<iterator>;

	constexpr span() noexcept = default;
	constexpr span(const span& rhs) noexcept = default;

	constexpr span(pointer ptr, index_type count) :
		impl::span_base<T, Extent>(count),
		ptr_(ptr)
	{}

	span(pointer first, pointer last) :
		impl::span_base<T, Extent>(std::distance(first, last)),
		ptr_(first)
	{
		assert(first < last);
		assert(std::distance(first, last) == this->count_);
	}

	template<class Container>
	constexpr span(Container& c) : span(c.data(), c.size()) {}

	template<class Container>
	constexpr span(const Container& c) : span(c.data(), c.size()) {}


	constexpr reference operator[] (index_type i) const {
		return
#if __clang__ == 1 || __GNUC__ >= 5
			assert(i < this->count_),
#endif
			ptr_[i];
	}

	constexpr pointer data() const noexcept { return ptr_; }
	constexpr index_type size() const noexcept { return this->count_; }
	constexpr index_type size_bytes() const noexcept {
		return this->count_ * sizeof(element_type);
	}

	constexpr iterator begin() const noexcept { return ptr_; }
	constexpr const_iterator cbegin() const noexcept { return ptr_; }

	constexpr iterator end() const noexcept { return ptr_ + this->count_; }
	constexpr const_iterator cend() const noexcept { return ptr_+this->count_; }


	pointer ptr_ = nullptr;
};


template<typename T, std::size_t N>
constexpr span<T, N> make_span(T (&array)[N]) noexcept
{
	return span<T, N>(array, N);
}

}

#endif
