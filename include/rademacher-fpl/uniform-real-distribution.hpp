// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_UNIFORM_REAL_DISTRIBUTION_HPP
#define RADEMACHER_FPL_UNIFORM_REAL_DISTRIBUTION_HPP

#include <cstddef>
#include <cstdint>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/traits.hpp>
#include <stdexcept>


namespace rademacher_fpl {
namespace urd_impl {

template<
	typename Unsigned,
	class Generator
>
Unsigned draw_exponent(Unsigned e_min, Unsigned e_max, Generator* p_generator);

template<
	typename Real,
	class Generator,
	typename Unsigned = traits::as_fast_unsigned_t<Real>
>
Unsigned draw_significand_like(Real, Generator* p_generator);



template<
	typename Real,
	class GeneratorE,
	class GeneratorS = GeneratorE,
	// this template parameter is intended to be used for development purposes
	typename Unsigned = traits::as_fast_unsigned_t<Real>
>
Real make_uniform_random_value(
	Real a, Real b,
	GeneratorE* p_generator_exponent,
	GeneratorS* p_generator_significand
);

template<typename Real, class Generator>
Real make_uniform_random_value(Real a, Real b, Generator* p_generator)
{
	return make_uniform_random_value(a, b, p_generator, p_generator);
}

}


template<typename Real>
struct uniform_real_distribution
{
	static_assert(
		std::is_floating_point<Real>::value, "Expected floating-point type"
	);


	using result_type = Real;


	uniform_real_distribution(Real a = 0, Real b = 1) :
		a_(a),
		b_(b)
	{
		if(isnan(a))
			throw std::invalid_argument("a is NaN");
		if(isnan(b))
			throw std::invalid_argument("b is NaN");

		if(a >= b)
			throw std::range_error("Range is empty");
	}


	template<class Generator>
	result_type operator() (Generator& generator)
	{
		return urd_impl::make_uniform_random_value(a_, b_, &generator);
	}


	Real a_;
	Real b_;
};

}

#endif
