// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_STATISTICS_HPP
#define RADEMACHER_FPL_STATISTICS_HPP

#include <cstdint>
#include <rademacher-fpl/span.hpp>
#include <rademacher-fpl/traits.hpp>
#include <vector>

namespace rademacher_fpl {

template<typename Real, typename Unsigned=std::uintmax_t>
struct draw_counter
{
	static_assert(std::is_floating_point<Real>::value, "");

	using real_t = Real;
	using uint_t = Unsigned;
	using bin_list = std::vector<uint_t>;

	static constexpr traits::as_fast_unsigned_t<Real> NUM_SIGNIFICANDS =
		traits::max_significand<Real>::value + 1u
	;

	/**
	 * @pre `a` and `b` describe a half-open interval.
	 *
	 * @param[in] alias_zero_p If true and if zero is in the interval `[a, b)`,
	 *                         then treat +0 and -0 as aliases.
	 * @param[in] accept_out_of_range_p If true, count values passed to
	 *            `add_draw` outside of `[a, b)`. Throw an exception otherwise.
	 */
	draw_counter(
		Real a, Real b, bool alias_zero_p=false, bool accept_out_of_range_p=false
	);
	draw_counter(const draw_counter&) = delete;
	draw_counter(draw_counter&& other) = default;

	real_t get_left_interval_bound() const { return l_; }
	real_t get_right_interval_bound() const;

	uint_t get_num_draws(real_t r) const;
	std::uintmax_t get_num_draws() const { return num_draws_; }

	span<const uint_t> get_subinterval(
		bool sign,
		traits::as_fast_unsigned_t<Real> exponent
	) const;

	void add_draw(real_t r, uint_t count=1u);

	std::pair<std::size_t, bin_list&> get_bin_(Real r);
	std::pair<std::size_t, const bin_list&> get_bin_(Real r) const;


	Real l_;
	Real r_; // `[l_, r_]` is a closed interval
	bool alias_zero_p_;
	bool accept_out_of_range_p_;
	std::uintmax_t num_draws_ = 0;
	bin_list unsigned_bins_;
	bin_list signed_bins_;
};


template<
	typename Real,
	typename Unsigned = traits::as_fast_unsigned_t<Real>,
	typename Float = double
>
Float compute_num_virtual_bins_r(Real a, Real b);

template<typename Real>
std::uintmax_t compute_num_virtual_bins(Real a, Real b);


template<
	typename Real,
	typename Unsigned = traits::as_fast_unsigned_t<Real>,
	typename Float = double
>
Float compute_uniform_cdf(Real a, Real b, bool sign, Unsigned exp);


template<typename Real, typename Unsigned=std::uintmax_t, typename Ret=double>
Ret compute_kolmogorov_smirnov_statistic(const draw_counter<Real, Unsigned>&);

/**
 * This function computes the minimum value of the two-sample
 * Kolmogorov-Smirnov test allowing one to reject the null hypothesis with the
 * given confidence level (H0: the samples are from the same distribution).
 *
 * @param[in] m The number of samples draw from the first distribution.
 * @param[in] n The number of samples draw from the second distribution.
 */
template<typename Real>
Real compute_ks_rejection_value(
	Real significance_level, std::uint64_t m, std::uint64_t n
);

}

#endif
