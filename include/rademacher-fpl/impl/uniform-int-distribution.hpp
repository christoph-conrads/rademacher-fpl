// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_IMPL_UNIFORM_INT_DISTRIBUTION_HPP
#define RADEMACHER_FPL_IMPL_UNIFORM_INT_DISTRIBUTION_HPP

#include <cassert>
#include <cstdlib>
#include <rademacher-fpl/bit.hpp>
#include <rademacher-fpl/uniform-int-distribution.hpp>
#include <random>
#include <type_traits>


namespace rademacher_fpl {
namespace impl
{

template<
	typename UInt,
	 typename Generator,
	 bool Pow2Range_p
>
struct draw_uniform_int_t
{
	static_assert(not std::is_signed<UInt>::value, "");
	static_assert(ispow2m1(std::numeric_limits<UInt>::max()), "");

	static UInt execute(Generator& generator, UInt a, UInt b)
	{
		using ResultT = typename Generator::result_type;
		using CommonType = typename std::common_type<UInt, ResultT>::type;
		using T = typename std::common_type<unsigned, CommonType>::type;

		auto range_out_m1 = UInt(b - a);

		if(not ispow2m1(range_out_m1))
		{
			auto dist = rademacher_fpl::uniform_int_distribution<UInt>(a, b);

			return dist(generator);
		}

		assert(Generator::min() == 0);

		const auto range_in_m1 = Generator::max();
		const auto num_bits_in = log2p1(range_in_m1);
		auto num_bits_out = log2p1(range_out_m1);
		auto num_iterations = (num_bits_out-1u) / num_bits_in;

		// the conditional expression silences compiler warnings (e.g., GCC 7.3)
		const auto shift = range_in_m1 < std::numeric_limits<T>::max()
			? log2p1(range_in_m1)
			: 0u
		;
		auto g = T{generator()};

		for(auto i = 0u; i < num_iterations; ++i)
			g = (g << shift) | T{generator()};

		auto ret = (g & range_out_m1) + a;

		assert(ret >= std::numeric_limits<UInt>::min());
		assert(ret <= std::numeric_limits<UInt>::max());

		return ret;
	}
};

}


template<typename UInt>
template<class Generator>
typename uniform_int_distribution<UInt>::result_type
uniform_int_distribution<UInt>::operator() (Generator& generator)
{
	static_assert(not std::is_signed<result_type>::value, "");
	static_assert(
		not std::is_signed<typename Generator::result_type>::value, ""
	);

	using CommonType =
		typename std::common_type<
			result_type, typename Generator::result_type
		>::type
	;
	// avoid unexpected conversion to int if both result types are smaller than
	// int
	using T = typename std::common_type<CommonType, unsigned>::type;

	const auto l = T{Generator::min()};
	const auto r = T{Generator::max()};
	const auto range_in_m1 = r - l;

	auto a = T{param().a()};
	auto b = T{param().b()};
	auto range_out_m1 = b - a;

	// The assertion below should not fail unless
	// * a generator class has static `min()`, `max()` methods without
	//   `constexpr` qualifier and
	// * the user provided an incorrect template specialization of
	//   `rademacher_fpl::has_pow2_range`.
	//
	// NB this case is handled by `draw_uniform_int`
	assert(not ispow2m1(range_in_m1) or not ispow2m1(range_out_m1));

	if(range_in_m1 == range_out_m1)
	{
		return generator() - l + a;
	}

	if(range_in_m1 > range_out_m1)
	{
		auto range_in = range_in_m1 + 1;
		auto range_out = range_out_m1 + 1;

		assert(range_out != 0);

		auto quotient = range_in == 0
			? range_in_m1 / range_out
			: range_in / range_out
		;
		auto min_reject_lr = quotient * range_out + l;
		auto x_lr = T{Generator::max()};

		do
			x_lr = generator();
		while(x_lr >= min_reject_lr);

		auto x = x_lr - l;
		auto x_ab = x % range_out + a;

		return x_ab;
	}

	auto dist = std::uniform_int_distribution<result_type>(a, b);

	return dist(generator);
}

}

#endif
