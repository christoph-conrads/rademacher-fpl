// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_IMPL_MATH_HPP
#define RADEMACHER_FPL_IMPL_MATH_HPP

#include <cassert>
#include <cerrno>
#include <cfenv>
#include <limits>
#include <rademacher-fpl/math.hpp>

namespace rademacher_fpl {
// The math.h function clones are implemented in a separate namespace to allow
// them being tested with built-in float types because at this point, we
// already defined the clone overloads for `float`, `double`, and `long
// double`.
namespace impl {

template<typename Real>
Real abs(Real f)
{
	static_assert(std::is_floating_point<Real>::value, "");

	auto g = assemble_like(f, false, exponent(f), significand(f));

	return g;
}


template<typename Real>
bool isfinite(Real f)
{
	return exponent(f) < traits::max_exponent<Real>::value;
}


template<typename Real>
bool isnan(Real f)
{
	return
		exponent(f) == traits::max_exponent<Real>::value and significand(f) != 0
	;
}


template<typename Real>
Real nextafter(Real x, Real d)
{
	namespace rfpl = rademacher_fpl;

	using uint_t = traits::as_fast_unsigned_t<Real>;

	if(rfpl::isnan(x))
		return x;
	if(rfpl::isnan(d))
		return d;
	if(x == d)
		return d;

	if(x == 0)
	{
		std::feraiseexcept(FE_INEXACT | FE_UNDERFLOW);

		return assemble_like(x, signbit(d), 0, 1);
	}

	const auto MAX = std::numeric_limits<Real>::max();

	if((x == MAX and x < d) or (x == -MAX and d < x))
	{
		if(math_errhandling & MATH_ERRNO)
			errno = ERANGE;

		std::feraiseexcept(FE_INEXACT | FE_OVERFLOW);

		// Christoph C. thinks one has to return `HUGE_VAL` here but this does
		// not match the behavior of `std::nextafter`.
		return x > 0
			? std::numeric_limits<Real>::infinity()
			: -std::numeric_limits<Real>::infinity()
		;
	}

	constexpr auto one = uint_t{1};
	constexpr auto NUM_SIGNIFICAND_DIGITS =
		traits::num_significand_digits<Real>::value;
	constexpr auto NUM_SIGNIFICANDS = one << NUM_SIGNIFICAND_DIGITS;
	constexpr auto MAX_SIGNIFICAND = (one << NUM_SIGNIFICAND_DIGITS) - one;

	auto towards_zero_p = signbit(x) == 0 ? d < x : x < d;

	auto e_x = exponent(x);
	auto s_x = significand(x);
	auto s_y =
		towards_zero_p and s_x == 0 ? MAX_SIGNIFICAND :
		towards_zero_p and s_x != 0 ? s_x - 1u :
		true                        ? (s_x + 1u) % NUM_SIGNIFICANDS : 0
	;
	auto e_y =
		towards_zero_p and s_x == 0 ? e_x - 1u :
		towards_zero_p and s_x != 0 ? e_x :
		s_y == 0                    ? e_x + 1u :
		true                        ? e_x      : 0
	;

	if(e_y == 0)
	{
		if(math_errhandling & MATH_ERRNO)
			errno = ERANGE;

		std::feraiseexcept(FE_INEXACT | FE_UNDERFLOW);
	}

	auto y = assemble_like(x, signbit(x), e_y, s_y);

	assert(x < d ? x < y : y < x);

	return y;
}



template<typename Real>
typename std::enable_if<std::is_floating_point<Real>::value, bool>::type
signbit(Real f)
{
	using UInt = traits::as_unsigned_t<Real>;

	constexpr auto n_e = traits::num_exponent_digits<Real>::value;
	constexpr auto n_s = traits::num_significand_digits<Real>::value;

	auto u = UInt{0};

	std::memcpy(&u, &f, sizeof(f));

	return config::BIG_ENDIAN_P ? u & 1u : u >> (n_e+n_s);
}


template<typename Real>
traits::as_unsigned_t<Real> exponent(Real f)
{
	using UInt = traits::as_unsigned_t<Real>;

	constexpr auto n_e = traits::num_exponent_digits<Real>::value;
	constexpr auto n_s = traits::num_significand_digits<Real>::value;
	constexpr auto one = UInt{1};
	constexpr auto MASK = (one << n_e) - 1u;

	auto u = traits::as_unsigned_t<Real>{0};

	std::memcpy(&u, &f, sizeof(f));

	return config::BIG_ENDIAN_P
		? (u >> 1) & MASK
		: (u >> n_s) & MASK
	;
}


template<typename Real>
traits::as_unsigned_t<Real> significand(Real f)
{
	using UInt = traits::as_unsigned_t<Real>;

	constexpr auto n_e = traits::num_exponent_digits<Real>::value;
	constexpr auto n_s = traits::num_significand_digits<Real>::value;
	constexpr auto one = UInt{1};
	constexpr auto MASK = (one << n_s) - 1u;

	auto u = traits::as_unsigned_t<Real>{0};

	std::memcpy(&u, &f, sizeof(f));

	return config::BIG_ENDIAN_P
		? u >> (n_e+1u)
		: u & MASK
	;
}



template<typename Real>
struct assembler
{
	static Real execute(
		bool sign_p,
		typename traits::as_unsigned_t<Real> exponent,
		typename traits::as_unsigned_t<Real> significand
	)
	{
		using UInt = typename traits::as_unsigned<Real>::type;

		assert(exponent <= traits::max_exponent<Real>::value);
		assert(significand <= traits::max_significand<Real>::value);

		if(exponent > traits::max_exponent<Real>::value)
			return std::numeric_limits<Real>::quiet_NaN();

		if(significand > traits::max_significand<Real>::value)
			return std::numeric_limits<Real>::quiet_NaN();

		constexpr auto n_e = traits::num_exponent_digits<Real>::value;
		constexpr auto n_s = traits::num_significand_digits<Real>::value;

		auto sign = sign_p ? UInt{1} : UInt{0};
		auto u = UInt(config::BIG_ENDIAN_P
			? (significand << (n_e+1)) | (exponent << 1) | sign
			: (sign << (n_e+n_s)) | (exponent << n_s) | significand
		);

		auto f = std::numeric_limits<Real>::quiet_NaN();

		std::memcpy(&f, &u, sizeof(f));

		return f;
	}
};

}

template<typename Real>
Real abs(Real f)
{
	return impl::abs(f);
}


template<typename Real>
bool isfinite(Real f)
{
	return impl::isfinite(f);
}


template<typename Real>
bool isnan(Real f)
{
	return impl::isnan(f);
}


template<typename Real>
Real nextafter(Real x, Real d)
{
	return impl::nextafter(x, d);
}


template<typename Real>
Real nextafter_0(Real x, Real d)
{
	static_assert(std::is_floating_point<Real>::value, "");

	if(isnan(x) or x != 0)
		return nextafter(x, d);

	if(x == d)
		return d;

	std::feraiseexcept(FE_INEXACT | FE_UNDERFLOW);

	auto sign_p = signbit(x);
	auto ret =
		sign_p     and d > x ? assemble_like(x, false, 0u, 0u) :
		sign_p     and d < x ? assemble_like(x, true,  0u, 1u) :
		not sign_p and d < x ? assemble_like(x, true,  0u, 0u) :
		not sign_p and d > x ? assemble_like(x, false, 0u, 1u) :
			std::numeric_limits<Real>::quiet_NaN()
	;

	return ret;
}



template<typename Real>
typename std::enable_if<std::is_floating_point<Real>::value, bool>::type
signbit(Real f)
{
	return impl::signbit(f);
}


template<typename Real>
traits::as_unsigned_t<Real> exponent(Real f)
{
	return impl::exponent(f);
}


template<typename Real>
traits::as_unsigned_t<Real> significand(Real f)
{
	return impl::significand(f);
}


template<typename Real>
Real assembler<Real>::execute(
	bool sign_p,
	typename traits::as_unsigned_t<Real> exponent,
	typename traits::as_unsigned_t<Real> significand
)
{
	return impl::assembler<Real>::execute(sign_p, exponent, significand);
}



template<typename Real>
typename traits::as_unsigned<Real>::type count_num_values_in(Real a, Real b)
{
	using Integer = typename traits::as_unsigned<Real>::type;

	constexpr auto MAX = std::numeric_limits<Integer>::max();

	assert(not isnan(a));
	assert(not isnan(b));

	auto f = count_num_values_in<Real>;

	if(isnan(a) or isnan(b))
		return MAX;
	if(b <= a)
		return 0;
	if(a < 0 and b <= 0)
		return f(-b, -a);
	if(a < 0 and b > 0)
		return f(Real{0}, -a) + f(Real{0}, b);

	if(exponent(a) == exponent(b))
		return significand(b) - significand(a);

	assert(0 <= a);
	assert(0 < b);
	assert(a < b);

	constexpr auto num_significand_digits =
		traits::num_significand_digits<Real>::value;
	constexpr auto num_significands = Integer{1} << num_significand_digits;

	auto e_a = exponent(a);
	auto e_b = exponent(b);
	auto s_a = significand(a);
	auto s_b = significand(b);
	auto q = e_b - e_a - Integer{1};

	auto num_bins_l = num_significands - s_a;
	auto num_bins_c = q * num_significands;
	auto num_bins_r = s_b;
	auto num_bins = num_bins_l + num_bins_c + num_bins_r;

	return num_bins;
}

}

#endif
