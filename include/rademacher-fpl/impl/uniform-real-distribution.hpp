// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_IMPL_UNIFORM_REAL_DISTRIBUTION_HPP
#define RADEMACHER_FPL_IMPL_UNIFORM_REAL_DISTRIBUTION_HPP

#include <cassert>
#include <limits>
#include <rademacher-fpl/bit.hpp>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/type-traits.hpp>
#include <rademacher-fpl/uniform-int-distribution.hpp>
#include <rademacher-fpl/uniform-real-distribution.hpp>
#include <random>
#include <type_traits>


#if __GNUC__
#define RADEMACHER_FPL_UNLIKELY(expr) __builtin_expect(!!(expr), 0)
#else
#define RADEMACHER_FPL_UNLIKELY(expr) expr
#endif


namespace rademacher_fpl {
namespace urd_impl {

template<typename T>
T fast_nextsmaller(T x)
{
	static_assert(std::is_floating_point<T>::value, "");

	using UInt = traits::as_unsigned_t<T>;
	using Integer = typename std::make_signed<UInt>::type;

	assert(x > 0 or rademacher_fpl::isfinite(x));

	auto i = Integer{0};

	std::memcpy(&i, &x, sizeof(x));

	i = i == 0 ? std::numeric_limits<Integer>::min() :
		i < 0  ? i+1 :
		i > 0  ? i-1 : -1
	;

	std::memcpy(&x, &i, sizeof(x));

	assert(not rademacher_fpl::isnan(x));

	return x;
}


template<typename T>
T nextsmaller(T x)
{
	return rademacher_fpl::nextafter(x, -std::numeric_limits<T>::infinity());
}

inline float nextsmaller(float x) { return fast_nextsmaller(x); }
inline double nextsmaller(double x) { return fast_nextsmaller(x); }




template<
	typename Unsigned,
	class Generator
>
Unsigned draw_exponent(Unsigned a, Unsigned b, Generator* p_generator)
{
	static_assert(std::is_integral<Unsigned>::value, "");
	static_assert(not std::is_signed<Unsigned>::value, "");

	namespace rfpl = rademacher_fpl;

	using result_t = typename Generator::result_type;
	using uint_t = rfpl::common_t<Unsigned, result_t>;

	static_assert(std::is_integral<result_t>::value, "");
	static_assert(not std::is_signed<result_t>::value, "");

	static_assert(not std::is_signed<uint_t>::value, "");

	// One may use small unsigned integer types to test the behavior when there
	// are more bins than digits. This assertion checks that we are not
	// /accidentally/ using an integer type wider than `Unsigned`.
	static_assert(
		std::numeric_limits<std::size_t>::digits
			< std::numeric_limits<result_t>::digits
		or std::is_same<Unsigned, uint_t>::value,
		""
	);

	assert(a < b);

	if(a + 1u == b)
		return a;


	constexpr auto zero = uint_t{0};
	constexpr auto one = uint_t{1};
	constexpr auto num_digits = uint_t(std::numeric_limits<uint_t>::digits);
	// the number of bins for *normal* numbers
	auto num_bins = b - std::max(Unsigned{1}, a) + 0u;
	auto denormal_p = a == 0;

	if(num_bins <= num_digits)
	{
		auto l = denormal_p ? zero : one;
		auto r = num_bins == num_digits
			? std::numeric_limits<uint_t>::max()
			: uint_t((one << num_bins) - 1u);
		auto virtual_bin = rfpl::impl::draw_uniform_int(p_generator, l, r);
		auto exponent = log2p1(virtual_bin) - (denormal_p ? 0u : 1u) + a;

		assert(exponent >= a);
		assert(exponent < b);

		return exponent;
	}

	// use only as many digits as returned by the generator (if it has a
	// power-of-two range) instead of drawing from all possible `uint_t` values.
	// this will speed up generators like `std::ranlux24` or generators
	// returning 32-bit values when `uint_t` is a 64-bit type.
	//
	// do not use constexpr here because of, e.g., boost generators
	const auto num_generator_digits = has_pow2_range<Generator>::value
		? rfpl::log2p1(uint_t(Generator::max() - Generator::min()))
		: num_digits
	;
	const auto d = num_generator_digits;

	while(true)
	{
		auto num_iterations = (num_bins-1u) / d;

		assert(num_bins > num_iterations * d);

		for(auto i = zero; i < num_iterations; ++i)
		{
			auto num_remaining_bins = num_bins - i * d;
			auto num_bins_per_group = num_remaining_bins - d;
			auto l = zero;
			auto r = has_pow2_range<Generator>::value
				? Generator::max()
				: std::numeric_limits<uint_t>::max()
			;
			auto group = rfpl::impl::draw_uniform_int(p_generator, l, r);

			if(group > 0)
			{
				auto bin =
					log2p1(group) - (denormal_p ? 0u : 1u) + num_bins_per_group;
				auto exponent = bin + a;

				assert(bin >= num_bins_per_group + (denormal_p ? 1u : 0u));
				assert(exponent >= a);
				assert(exponent < b);

				return exponent;
			}
		}

		auto num_remaining_bins = num_bins - num_iterations * d;

		assert(num_remaining_bins <= num_digits);
		assert(num_remaining_bins <= d);

		auto l = zero;
		auto r = num_remaining_bins == num_digits
			? std::numeric_limits<uint_t>::max()
			: uint_t((one << num_remaining_bins) - 1u);
		auto virtual_bin = rfpl::impl::draw_uniform_int(p_generator, l, r);

		if(virtual_bin > 0 or denormal_p)
		{
			auto bin = log2p1(virtual_bin) - (denormal_p ? 0u : 1u);
			auto exponent = bin + a;

			assert(exponent >= a);
			assert(exponent < b);

			return exponent;
		}
	}
}


template<
	typename Real,
	class Generator,
	typename Unsigned
>
Unsigned draw_significand_like(Real, Generator* p_generator)
{
	assert(p_generator);

	using uint_t = Unsigned;
	using rademacher_fpl::impl::draw_uniform_int;

	constexpr auto one = uint_t{1};
	constexpr auto d = traits::num_significand_digits<Real>::value;
	constexpr auto l = uint_t{0};
	constexpr auto r = uint_t((one << d) - 1u);

	static_assert(d <= std::numeric_limits<uint_t>::digits, "");

	return draw_uniform_int(p_generator, l, r);
}



template<
	typename Real,
	class GeneratorE,
	class GeneratorS,
	typename Unsigned
>
Real make_uniform_random_value(
	Real a, Real b,
	GeneratorE* p_generator_exponent,
	GeneratorS* p_generator_significand
)
{
	static_assert(
		std::is_floating_point<Real>::value, ""
	);

	using uint_t = Unsigned;
	using rademacher_fpl::impl::draw_uniform_int;

	assert(p_generator_exponent);
	assert(p_generator_significand);

	constexpr auto num_digits =
		static_cast<uint_t>(std::numeric_limits<uint_t>::digits);
	constexpr auto num_significand_digits =
		uint_t{traits::num_significand_digits<Real>::value};

	static_assert(num_digits > num_significand_digits, "");

	if(RADEMACHER_FPL_UNLIKELY(
		not p_generator_exponent
		or not p_generator_significand
		or a >= b
		or isnan(a)
		or isnan(b)
		))
	{
		return std::numeric_limits<Real>::quiet_NaN();
	}

	constexpr auto zero = uint_t{0};
	constexpr auto one = uint_t{1};
	constexpr auto max_exponent = traits::max_exponent<Real>::value;
	constexpr auto max_significand = traits::max_significand<Real>::value;

	auto z = nextsmaller(b);

	// case 1
	if(a < 0 && z <= 0)
	{
		namespace rfpl = rademacher_fpl;

		const auto inf = std::numeric_limits<Real>::infinity();

		auto x = rfpl::abs(z);
		auto y = rfpl::nextafter(rfpl::abs(a), inf);
		auto c = make_uniform_random_value(
			x, y, p_generator_exponent, p_generator_significand
		);

		return -c;
	}

	// case 2
	if(a == -z)
	{
		auto m = make_uniform_random_value(
			Real{0}, b, p_generator_exponent, p_generator_significand
		);
		auto sign = draw_uniform_int(p_generator_significand, zero, one);

		return sign == 0 ? m : -m;
	}

	// case 3
	if(a >= 0 and exponent(a) == exponent(z))
	{
		auto l = significand(a);
		auto r = significand(z);
		auto significand = draw_uniform_int(p_generator_significand, l, r);

		return assemble_like(a, exponent(a), significand);
	}

	// case 4
	if(a >= 0 and significand(a) == 0 and significand(b) == 0)
	{
		assert(exponent(b) >= exponent(a) + 1);

		auto new_significand = draw_significand_like(a, p_generator_significand);
		auto exponent_a = uint_t{exponent(a)};
		auto exponent_b = uint_t{exponent(b)};
		auto new_exponent =
			draw_exponent(exponent_a, exponent_b, p_generator_exponent);

		return assemble_like(a, new_exponent, new_significand);
	}

	if(a >= 0)
	{
		assert(exponent(a) < exponent(b));

		constexpr auto n_s = one << num_significand_digits;

		auto a_denormal_p = exponent(a) == 0;
		auto e_a = uint_t{exponent(a)};
		auto e_b = uint_t{exponent(b)};
		auto p = uint_t(e_b - e_a);
		auto q = p - (a_denormal_p ? one : zero);

		auto n_l = n_s - significand(a);
		auto n_c = n_s;
		auto n_r = significand(b);

		if(q + num_significand_digits < num_digits)
		{
			auto w_l = 1;
			auto w_c = (one << q) - (a_denormal_p ? one : uint_t{2});
			auto w_r = one << q;

			auto t_l = n_l * w_l;
			auto t_c = n_c * w_c + t_l;
			auto t_r = n_r * w_r + t_c;

			auto l = zero;
			auto r = uint_t(t_r - 1u);
			const auto v = draw_uniform_int(p_generator_significand, l, r);

			constexpr auto e_max = uint_t{traits::max_exponent<Real>::value};

			auto compute_e_c =
				[a_denormal_p, e_a, t_l] (uint_t v) -> uint_t {
				auto shift = num_significand_digits + (a_denormal_p ? 0 : 1);
				auto w = uint_t(((v - t_l) >> shift) + 1);
				return e_a + log2p1(w);
			};
			auto s_c =
				             v < t_l ? v + significand(a) :
				t_l <= v and v < t_c ? uint_t((v - t_l) & (n_s - 1)) :
				t_c <= v and v < t_r ? uint_t((v - t_c) >> q) :
				true                 ? assert(false), one : one
			;
			auto e_c =
				             v < t_l ? e_a :
				t_l <= v and v < t_c ? compute_e_c(v) :
				t_c <= v and v < t_r ? e_b :
				true                 ? assert(false), e_max : e_max
			;

			auto c = assemble_like(a, e_c, s_c);

			assert(c >= a);
			assert(c < b);

			return c;
		}

		if(a_denormal_p)
		{
			auto l = zero;
			auto r = uint_t(n_s + n_r - 1u);

			while(true)
			{
				auto x = draw_uniform_int(p_generator_significand, l, r);
				auto s_c = x - (x < n_s ? zero : n_s);
				auto e_c = x < n_s
					? draw_exponent(e_a, e_b, p_generator_exponent)
					: e_b;

				if(e_c == e_a and s_c < significand(a))
					continue;

				auto c = assemble_like(a, e_c, s_c);

				return c;
			}
		}

		auto l = zero;
		auto r = uint_t(n_s + n_r - 1u);

		while(true)
		{
			auto vbin = draw_uniform_int(p_generator_significand, l, r);
			auto right_bin_p = vbin >= n_s;

			if(right_bin_p)
				return assemble_like(b, e_b, vbin-n_s);

			auto s = zero;
			auto t = uint_t(p + one);
			auto e_r = draw_exponent(s, t, p_generator_exponent);
			auto s_c = vbin;

			if(e_r == 0)
				continue;

			assert(e_r >= 1);
			assert(e_r <= p);

			auto e_c = e_r + e_a - 1;

			if(e_c == e_a and s_c < significand(a))
				continue;

			auto c = assemble_like(a, e_c, s_c);

			return c;
		}
	}

	assert(a < 0 and rademacher_fpl::nextafter(b, a) > 0);

	auto e_a = exponent(a);
	auto e_z = exponent(z);
	auto e_max = std::max(e_a, e_z);
	auto n_l = significand(a) + 1u;
	auto n_c = max_significand + 1u;
	auto n_r = significand(b);

	if(e_max + num_significand_digits + 2u < num_digits)
	{
		auto w_1 = e_a == 0 ? one : one << (e_a - 1u);
		auto w_0 = e_z == 0 ? one : one << (e_z - 1u);

		auto t_l = n_l * w_1;
		auto t_1 = t_l + (e_a > 0 ? n_c * w_1 : 0u);
		auto t_0 = t_1 + (e_z > 0 ? n_c * w_0 : 0u);
		auto t_r = t_0 + n_r * w_0;

		assert(t_l > 0);
		assert(t_1 >= t_l);
		assert(t_0 >= t_1);
		assert(t_r > t_0);
		assert(t_l == t_1 or e_a > 0);
		assert(t_1 == t_0 or e_z > 0);

		auto l = zero;
		auto r = uint_t(t_r - 1u);
		auto vbin = draw_uniform_int(p_generator_significand, l, r);

		auto signbit_c = vbin < t_1;
		auto e_c =
			vbin < t_l ? e_a :
			vbin < t_1 ? log2p1((vbin-t_l+1) & (w_1 - 1u)) :
			vbin < t_0 ? log2p1((vbin - t_1) & (w_0 - 1u)) :
			vbin < t_r ? e_z :
            true       ? assert(false), max_exponent : 0u
		;
		auto s_c =
			vbin < t_l ? (vbin - 0)   / w_1 :
			vbin < t_1 ? (vbin - t_l) / w_1 :
			vbin < t_0 ? (vbin - t_1) / w_0 :
			vbin < t_r ? (vbin - t_0) / w_0 :
			true       ? assert(false), max_significand : 0u
		;
		auto c = assemble_like(a, signbit_c, e_c, s_c);

		assert(c >= a);
		assert(c < b);

		return c;
	}

	assert(e_a > 0);
	assert(e_z > 0);

	auto n_a = significand(a) + 1u;
	auto n_1 = e_a == 0 ? 0u : max_significand + 1u;
	auto n_0 = e_z == 0 ? 0u : max_significand + 1u;
	auto n_z = significand(z) + 1u;

	auto restart_p = true;
	auto vbin_s = zero;
	auto signbit_c = false;

	while(restart_p)
	{
		vbin_s = std::numeric_limits<uint_t>::max();

		// decide on signbit
		auto e_diff = e_a > e_z ? e_a - e_z : e_z - e_a;
		auto vbin_signbit = std::numeric_limits<uint_t>::max();
		auto num_iterations = (e_diff+1u + num_digits-1u) / num_digits;

		for(auto i = one; i <= num_iterations; ++i)
		{
			auto l = zero;
			auto r = i == 1 and (e_diff+1u) % num_digits != 0
				? uint_t((1u << ((e_diff+1u) % num_digits)) - 1u)
				: std::numeric_limits<uint_t>::max()
			;

			vbin_signbit = draw_uniform_int(p_generator_exponent, l, r);

			if(i == 1 and vbin_signbit > r/2u + 1u)
			{
				i = zero;
				continue;
			}

			if(vbin_signbit != 0)
				break;
		}

		assert(vbin_signbit == 0 or vbin_signbit == 1 or e_a != e_z);

		signbit_c =
			(e_a == e_z and vbin_signbit==1)
			or (e_a > e_z and vbin_signbit != 0)
			or (e_z > e_a and vbin_signbit == 0)
		;

		auto l_s = zero;
		auto r_s = uint_t(std::max(n_a + n_1, n_0 + n_z) - 1u);

		vbin_s = draw_uniform_int(p_generator_significand, l_s, r_s);
		restart_p =
			(signbit_c and vbin_s >= n_a + n_1)
			or (not signbit_c and vbin_s >= n_0 + n_z)
		;
	}

	assert(vbin_s < n_a + n_1 or not signbit_c);
	assert(vbin_s < n_z + n_0 or signbit_c);
	assert(vbin_s <  n_a or e_a >  0 or not signbit_c);
	assert(vbin_s <  n_z or e_z >  0 or signbit_c);

	auto significand_c =
		    signbit_c and vbin_s <  n_a ? vbin_s :
		    signbit_c and vbin_s >= n_a ? vbin_s - n_a :
		not signbit_c and vbin_s <  n_z ? vbin_s :
		not signbit_c and vbin_s >= n_z ? vbin_s - n_z :
		true                                  ? max_significand + 1u : zero
	;
	auto draw_exp = [p_generator_exponent] (uint_t l, uint_t r) -> uint_t {
		return draw_exponent(l, r, p_generator_exponent);
	};
	auto exponent_c =
		    signbit_c and vbin_s <  n_a ? e_a :
		    signbit_c and vbin_s >= n_a ? draw_exp(zero, e_a) :
		not signbit_c and vbin_s <  n_z ? e_z :
		not signbit_c and vbin_s >= n_z ? draw_exp(zero, e_z) :
		true                              ? max_exponent + 1u : zero
	;
	auto c = assemble_like(a, signbit_c, exponent_c, significand_c);

	assert(a <= c);
	assert(c < b);

	return c;
}

}
}

#endif
