// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_IMPL_STATISTICS_HPP
#define RADEMACHER_FPL_IMPL_STATISTICS_HPP

#include <cassert>
#include <numeric>
#include <rademacher-fpl/math.hpp>
#include <rademacher-fpl/statistics.hpp>
#include <stdexcept>
#include <type_traits>

namespace rademacher_fpl {

template<typename Real, typename Unsigned>
draw_counter<Real, Unsigned>::draw_counter(
	Real a, Real b, bool alias_zero_p, bool accept_out_of_range_p
) :
	l_(a),
	r_(nextafter_0(b, a)),
	alias_zero_p_(alias_zero_p),
	accept_out_of_range_p_(accept_out_of_range_p)
{
	if(a >= b and signbit(a) == signbit(b))
		throw std::invalid_argument("Empty interval");

	assert(l_ <= r_);

	auto e_l = std::size_t{exponent(l_)};
	auto e_r = std::size_t{exponent(r_)};
	auto e_min = std::min(e_l, e_r);
	auto e_max = std::max(e_l, e_r);
	auto same_sign_p = signbit(l_) == signbit(r_);
	auto multiplier_s = same_sign_p ? 0u : e_l + 1u;
	auto multiplier_u = same_sign_p ? e_max - e_min + 1u : e_r + 1u;
	auto num_signed_bins = multiplier_s * NUM_SIGNIFICANDS;
	auto num_unsigned_bins = multiplier_u * NUM_SIGNIFICANDS;

	signed_bins_.resize(num_signed_bins);
	unsigned_bins_.resize(num_unsigned_bins);
}



template<typename Real, typename Unsigned>
Real draw_counter<Real, Unsigned>::get_right_interval_bound() const
{
	const auto inf = std::numeric_limits<Real>::infinity();

	return nextafter_0(r_, inf);
}



template<typename Real, typename Unsigned>
Unsigned draw_counter<Real, Unsigned>::get_num_draws(Real x) const
{
	auto pair = get_bin_(x);
	auto index = pair.first;
	auto& bins = pair.second;

	return bins.at(index);
}



template<typename Real, typename Unsigned>
std::pair<std::size_t, typename draw_counter<Real, Unsigned>::bin_list&>
draw_counter<Real, Unsigned>::get_bin_(Real x)
{
	const auto& self = *this;
	auto p = self.get_bin_(x);
	auto& bin = &p.second == &unsigned_bins_ ? unsigned_bins_ : signed_bins_;

	using return_t = decltype(get_bin_(x));

	return return_t(p.first, bin);
}



template<typename Real, typename Unsigned>
std::pair<std::size_t, const typename draw_counter<Real, Unsigned>::bin_list&>
draw_counter<Real, Unsigned>::get_bin_(Real x) const
{
	if(x < l_ or x > r_)
		throw std::invalid_argument("Value outside of interval");

	auto same_sign_p = signbit(l_) == signbit(r_);
	auto e_l = exponent(l_);
	auto e_r = exponent(r_);
	auto e_min = std::min(e_l, e_r);
	auto e_x = exponent(x);

	auto multiplier = same_sign_p ? e_x - e_min : e_x;
	auto index = multiplier * NUM_SIGNIFICANDS + significand(x);
	auto& bins = same_sign_p or not signbit(x) ? unsigned_bins_ : signed_bins_;

	assert(index < bins.size());

	using ret_t = decltype(get_bin_(x));

	return ret_t(std::size_t(index), bins);
}


template<typename Real, typename Unsigned>
span<const Unsigned>
draw_counter<Real, Unsigned>::get_subinterval(
	bool sign_p, traits::as_fast_unsigned_t<Real> exp
) const
{
	if(exp > traits::max_exponent<Real>::value)
		throw std::domain_error("The argument is not an exponent");

	auto same_sign_p = signbit(l_) == signbit(r_);

	if(same_sign_p and signbit(l_) != sign_p)
		throw std::out_of_range("Invalid signbit");

	auto e_min = same_sign_p ? std::min(exponent(l_), exponent(r_)) : 0u;
	auto e_max =
		same_sign_p ? std::max(exponent(l_), exponent(r_)) :
		not sign_p  ? exponent(r_) :
		sign_p      ? exponent(l_) : 0u
	;

	if(exp < e_min or exp > e_max)
		std::out_of_range("Exponent out of range");

	auto s_min =
		same_sign_p and     sign_p and exp == exponent(r_) ? significand(r_) :
		same_sign_p and not sign_p and exp == exponent(l_) ? significand(l_) :
		true                                              ? 0u : 0u
	;
	auto s_max =
		sign_p     and exp == exponent(l_) ? significand(l_) :
		not sign_p and exp == exponent(r_) ? significand(r_) :
		true                         ? traits::max_significand<Real>::value : 0u
	;

	assert(s_max >= s_min);
	assert(s_max < NUM_SIGNIFICANDS);

	auto& bins = same_sign_p or not sign_p ? unsigned_bins_ : signed_bins_;
	auto multiplier = exp - e_min;
	auto significand_count = s_max - s_min + 1u;
	// explicit conversion avoids accidental overflow
	auto offset =
		std::size_t{multiplier} * std::size_t{NUM_SIGNIFICANDS} + s_min
	;

	return span<const uint_t>(bins.data() + offset, significand_count);
}


template<typename Real, typename Unsigned>
void draw_counter<Real, Unsigned>::add_draw(Real x, uint_t count)
{
	if(count == 0)
		throw std::invalid_argument("Zero draw count");

	if(x < l_ or x > r_)
	{
		if(accept_out_of_range_p_)
		{
			num_draws_ += count;
			return;
		}

		throw std::invalid_argument("Value outside of interval");
	}

	if(x == 0 and alias_zero_p_)
	{
		assert(unsigned_bins_.size() > 0);

		if(signed_bins_.size() > 0)
		{
			signed_bins_[0] += count;
			num_draws_ += count;
		}

		unsigned_bins_[0] += count;
		num_draws_ += count;

		return;
	}

	auto pair = get_bin_(x);
	auto index = pair.first;
	auto& bins = pair.second;

	bins.at(index) += count;
	num_draws_ += count;
}



namespace impl {

template<typename T, typename Unsigned, typename Float>
std::pair<Float, Unsigned> compute_num_virtual_bins_r(T l, T r, Unsigned exp)
{
	namespace rfpl = rademacher_fpl;

	assert(l <= r);
	assert(l != r);

	auto self = compute_num_virtual_bins_r<T, Unsigned, Float>;

	auto e_l = rfpl::exponent(l);
	auto e_r = rfpl::exponent(r);
	auto e_min = std::min(e_r, e_l);
	auto e_max = std::max(e_r, e_l);

	assert(exp <= e_max);

	if(exp > e_max)
		return self(l, r, exp-1u);

	constexpr auto n_s = traits::max_significand<T>::value + 1u;

	assert(e_r < e_l or not rfpl::signbit(r));
	assert(e_l < e_r or rfpl::signbit(l));

	// num bins, sign bit set
	auto num_bins_1 =
		rfpl::signbit(r) and exp <  e_r ? 0u :
		rfpl::signbit(r) and exp == e_r ? n_s - rfpl::significand(r) :
		rfpl::signbit(l) and exp <  e_l ? n_s :
		rfpl::signbit(l) and exp == e_l ? rfpl::significand(l) + 1u : 0u
	;
	// num bins, sign bit unset
	auto num_bins_0 =
		not rfpl::signbit(l) and exp <  e_l ? 0u :
		not rfpl::signbit(l) and exp == e_l ? n_s - rfpl::significand(l) :
		not rfpl::signbit(r) and exp <  e_r ? n_s :
		not rfpl::signbit(r) and exp == e_r ? rfpl::significand(r) + 1u : 0u
	;
	auto num_bins = num_bins_0 + num_bins_1;

	if(exp == 0 or (rfpl::signbit(l) == rfpl::signbit(r) and exp == e_min))
	{
		auto num_virtual_bins = Float(num_bins);
		auto next_virtual_bin_exponent = exp == 0 ? 0u : 1u;

		return std::make_pair(num_virtual_bins, next_virtual_bin_exponent);
	}

	auto pair = self(l, r, exp-1u);
	auto old_num_virtual_bins = pair.first;
	auto virtual_bin_exponent = pair.second;
	auto num_virtual_bins =
		old_num_virtual_bins + ldexp(Float(num_bins), virtual_bin_exponent)
	;

	return std::make_pair(num_virtual_bins, virtual_bin_exponent+1);
}



template<typename T, typename Unsigned, typename Float>
std::pair<Float, std::uintmax_t>
compute_kolmogorov_smirnov_statistic(
	const draw_counter<T, Unsigned>& dc,
	bool sign_p, traits::as_fast_unsigned_t<T> exp
)
{
	static_assert(std::is_floating_point<T>::value, "");
	static_assert(std::is_floating_point<Float>::value, "");

	namespace rfpl = rademacher_fpl;

	using uint_t = std::uintmax_t;

	auto self = compute_kolmogorov_smirnov_statistic<T, Unsigned, Float>;

	auto a = dc.get_left_interval_bound();
	auto b = dc.get_right_interval_bound();
	auto l = a;
	auto r = rfpl::nextafter_0(b, a);

	if(not sign_p and r <= 0)
		return std::make_pair(Float{0}, uint_t{0});

	if(sign_p and l >= 0)
		return std::make_pair(Float{0}, uint_t{0});

	auto same_sign_p = rfpl::signbit(l) == rfpl::signbit(r);
	auto x =
		same_sign_p and r > 0 ? l :
		same_sign_p           ? rfpl::abs(r) :
		true                  ? T{0} : T{-1}
	;
	auto y =
		same_sign_p and r > 0 ? r :
		same_sign_p           ? rfpl::abs(l) :
		not sign_p            ? r :
		sign_p                ? rfpl::abs(l) : T{-1}
	;

	assert(x >= 0);
	assert(x < y);

	auto e_min = rfpl::exponent(x);
	auto e_max = rfpl::exponent(y);

	if(exp < e_min)
		return std::make_pair(Float{0}, uint_t{0});

	auto pair = exp == e_min
		? std::make_pair(Float{0}, uint_t{0})
		: self(dc, sign_p, exp-1)
	;

	if(exp > e_max)
		return pair;

	auto d_n = pair.first;
	auto n_0 = pair.second;
	auto ns = dc.get_subinterval(sign_p, exp);
	auto n_cum = std::accumulate(ns.cbegin(), ns.cend(), uint_t{0});
	auto n_1 = n_0 + n_cum;
	auto f_n = n_0;
	auto p = exp == e_min ? Float{0} : compute_uniform_cdf(a, b, sign_p, exp-1);
	auto q = compute_uniform_cdf(a, b, sign_p, exp);

	assert(std::isfinite(p));
	assert(std::isfinite(q));
	assert(p >= 0);
	assert(p < q);
	assert(q <= 1);

#ifndef NDEBUG
	constexpr auto NUM_SIGNIFICANDS = traits::max_significand<T>::value + 1u;

	auto first = exp == e_min ? rfpl::significand(x) : std::size_t{0};
	auto last = exp == e_max
		? rfpl::significand(y) + std::size_t{1}
		: std::size_t{NUM_SIGNIFICANDS}
	;

	assert(first < last);
	assert(last - first == ns.size());
#endif

	constexpr auto one = Float{1};

	for(auto i = std::size_t{0}; i < ns.size(); ++i)
	{
		f_n += ns[i];

		auto r = (i+1)/Float(ns.size());
		auto f = ((one - r) * p + r * q) * dc.get_num_draws();

		assert(f <= dc.get_num_draws());

		d_n = std::max(d_n, std::abs(f - f_n));
	}

	assert(f_n == n_1);

	return std::make_pair(d_n, n_1);
}

}



template<typename Real, typename Unsigned, typename Float>
Float compute_num_virtual_bins_r(Real a, Real b)
{
	assert(isfinite(a));
	assert(not isnan(b));

	bool a_less_b_p =
		(a < b)
		or (a == 0 and b == 0 and signbit(a) and not signbit(b))
	;

	constexpr auto nan = std::numeric_limits<Float>::quiet_NaN();

	if(not isfinite(a) or isnan(b) or not a_less_b_p)
		return nan;

	const auto max = std::numeric_limits<Real>::max();
	const auto inf = std::numeric_limits<Real>::infinity();

	assert(nextafter(max, inf) == inf);
	(void) max;
	(void) inf;

	auto l = a;
	auto r = nextafter_0(b, a);

	assert(l <= r);

	if(l == r)
		return signbit(l) == signbit(r) ? 1 : 2;

	if(signbit(l) == signbit(r) and exponent(l) == exponent(r))
	{
		return signbit(r)
			? significand(l) - significand(r) + 1u
			: significand(r) - significand(l) + 1u
		;
	}

	auto e_max = std::max(exponent(l), exponent(r));
	auto pair =
		impl::compute_num_virtual_bins_r<Real, Unsigned, Float>(l, r, e_max);
	auto num_virtual_bins = pair.first;

	assert(std::isfinite(num_virtual_bins));
	assert(num_virtual_bins > 0);

	return num_virtual_bins;
}


template<typename Real>
std::uintmax_t compute_num_virtual_bins(Real a, Real b)
{
	using uint_t = std::uintmax_t;

	const auto inf = std::numeric_limits<Real>::max();
	constexpr auto max = std::numeric_limits<uint_t>::max();
	constexpr auto one = uint_t{1};

	assert(not isnan(a));
	assert(not isnan(b));

	bool a_less_b_p =
		(a < b)
		or (a == 0 and b == 0 and signbit(a) and not signbit(b))
	;

	if(not isfinite(a) or isnan(b) or not a_less_b_p)
		return max;

	auto f = compute_num_virtual_bins<Real>;
	auto l = a;
	auto r = nextafter_0(b, a);

	if(l == r)
		return signbit(l) == signbit(r) ? 1u : 2u;

	if(signbit(l) and signbit(r))
		return f(abs(r), nextafter_0(abs(l), inf));

	if(signbit(l) and not signbit(r))
		return f(Real{0}, nextafter_0(abs(l), inf)) + f(Real{0}, b);

	assert(l >= 0);
	assert(0 < r);
	assert(l < r);
	assert(not signbit(l));
	assert(not signbit(r));

	if(exponent(l) == exponent(r))
	{
		assert(significand(r) >= significand(l));

		return significand(r) - significand(l) + one;
	}

	constexpr auto num_significand_digits =
		traits::num_significand_digits<Real>::value;
	constexpr auto num_significands = one << num_significand_digits;

	auto e_a = exponent(a);
	auto e_b = exponent(b);
	auto s_a = significand(a);
	auto s_b = significand(b);
	auto a_denormal_p = exponent(a) == 0;
	auto q = e_b - e_a - one;
	auto k = a_denormal_p ? uint_t{1} : uint_t{2};

	if(q + num_significand_digits + 2 >= std::numeric_limits<uint_t>::digits)
		return max;

	auto num_bins_l = num_significands - s_a;
	auto num_bins_c = k * ((one << q) - one) * num_significands;
	auto num_bins_r = k * (one << q) * s_b;
	auto num_bins = num_bins_l + num_bins_c + num_bins_r;

	return num_bins;
}



template<typename Real, typename Unsigned, typename Float>
Float compute_uniform_cdf(Real a, Real b, bool sign_p, Unsigned exp)
{
	static_assert(std::is_floating_point<Real>::value, "");
	static_assert(std::is_integral<Unsigned>::value, "");
	static_assert(not std::is_signed<Unsigned>::value, "");
	static_assert(std::is_floating_point<Float>::value, "");

	assert(isfinite(a));
	assert(not isnan(b));

	constexpr auto nan = std::numeric_limits<Float>::quiet_NaN();

	bool a_less_b_p =
		(a < b)
		or (a == 0 and b == 0 and signbit(a) and not signbit(b))
	;

	if(not isfinite(a) or isnan(b) or not a_less_b_p)
		return nan;

	auto l = a;
	auto r = nextafter_0(b, a);
	auto e_l = exponent(l);
	auto e_r = exponent(r);

	if(signbit(r) and not sign_p)
		return nan;
	if(not signbit(l) and sign_p)
		return nan;

	constexpr auto s_max = traits::max_significand<Real>::value;

	auto x =
		sign_p     and exp < e_l      ? assemble_like(a, 1, exp, s_max) :
		sign_p     and exp >= e_l     ? a :
		not sign_p and signbit(l)     ? Real{0} :
		not sign_p and not signbit(l) ? a : b
	;
	auto y =
		not sign_p and exp < e_r      ? assemble_like(a, 0, exp+1, 0) :
		not sign_p and exp >= e_r     ? b :
		sign_p     and not signbit(r) ? Real{0} :
		sign_p     and signbit(r)     ? b : a
	;

	auto f = compute_num_virtual_bins_r<Real, Unsigned, Float>;
	auto nominator = f(x, y);
	auto denominator = f(a, b);

	// If the denominator is infinite, this function would return zero most of
	// the time which would be grossly misleading. Hence, return not-a-number
	// instead.
	return std::isinf(denominator) ? nan : nominator / denominator;
}



template<typename T, typename Unsigned, typename Float>
Float compute_kolmogorov_smirnov_statistic(const draw_counter<T, Unsigned>& dc)
{
	static_assert(std::is_integral<Unsigned>::value, "");
	static_assert(not std::is_signed<Unsigned>::value, "");
	static_assert(std::is_floating_point<Float>::value, "");

	if(dc.get_num_draws() == 0)
		return std::numeric_limits<Float>::quiet_NaN();

	auto f = impl::compute_kolmogorov_smirnov_statistic<T, Unsigned, Float>;
	auto l = dc.get_left_interval_bound();
	auto r = nextafter_0(dc.get_right_interval_bound(), l);
	auto d_1 = Float{0};
	auto d_0 = Float{0};
	auto num_draws = typename draw_counter<T>::uint_t{0};

	if(l < 0)
	{
		auto pair = f(dc, 1, exponent(l));

		d_1 = pair.first;
		num_draws += pair.second;
	}
	if(r > 0)
	{
		auto pair = f(dc, 0, exponent(r));

		d_0 = pair.first;
		num_draws += pair.second;
	}

	assert(std::isfinite(d_1));
	assert(d_1 >= 0);

	assert(std::isfinite(d_0));
	assert(d_0 >= 0);

	assert(num_draws <= dc.num_draws_);

	return std::max(d_0, d_1) / dc.get_num_draws();
}



template<typename Real>
Real compute_ks_rejection_value(
	Real significance_level, std::uint64_t m, std::uint64_t n
)
{
	static_assert(std::is_floating_point<Real>::value, "");

	constexpr auto nan = std::numeric_limits<Real>::quiet_NaN();
	auto alpha = significance_level;

	if(not std::isfinite(alpha) or alpha <= 0 or alpha > 1)
		return nan;

	if(n == 0 or m == 0)
		return nan;

	auto c = std::sqrt(-std::log(alpha) / Real{2});
	auto x = Real(m);
	auto y = Real(n);
	auto r = c * std::sqrt(x+y)/(std::sqrt(x)*std::sqrt(y));

	return r;
}

}

#endif
