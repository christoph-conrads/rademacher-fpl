// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_LITERALS_HPP
#define RADEMACHER_FPL_LITERALS_HPP

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>

namespace rademacher_fpl {
	namespace literals {
		constexpr std::size_t operator "" _z (unsigned long long n)
		{
			return
#if __clang__ == 1 || __GNUC__ >= 5
				assert(n <= std::numeric_limits<std::size_t>::max()),
#endif
				n;
		}

		constexpr std::uintmax_t operator "" _u (unsigned long long n)
		{
			return n;
		}
	}
}

#endif
