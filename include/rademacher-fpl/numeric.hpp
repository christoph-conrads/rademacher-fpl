// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_NUMERIC_HPP
#define RADEMACHER_FPL_NUMERIC_HPP

#include <type_traits>

namespace rademacher_fpl {

template<typename T>
constexpr T gcd(T m, T n)
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	return n == 0 ? m : gcd(n, m % n);
}

template<typename T, typename... Args>
constexpr T gcd(T m, T n, Args... args)
{
	return gcd(gcd(m, n), args...);
}

}

#endif
