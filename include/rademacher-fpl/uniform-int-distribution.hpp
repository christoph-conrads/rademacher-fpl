// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_UNIFORM_INT_DISTRIBUTION_HPP
#define RADEMACHER_FPL_UNIFORM_INT_DISTRIBUTION_HPP

#include <cassert>
#include <limits>
#include <rademacher-fpl/bit.hpp>
#include <stdexcept>
#include <type_traits>


namespace rademacher_fpl {

template<typename Generator> struct has_pow2_range;
template<typename UInt> struct uniform_int_distribution;


namespace impl
{

template<
	typename UInt,
	 typename Generator,
	 bool Pow2Range_p = has_pow2_range<Generator>::value
>
struct draw_uniform_int_t;


template<typename T, typename Generator>
struct draw_uniform_int_t<T, Generator, false>
{
	static T execute(Generator& gen, T a, T b)
	{
		auto dist = rademacher_fpl::uniform_int_distribution<T>(a, b);

		return dist(gen);
	}
};


template<typename T, typename Generator>
T draw_uniform_int(
	Generator* p_generator, T a, T b=std::numeric_limits<T>::max()
)
{
	assert(p_generator);

	return draw_uniform_int_t<T, Generator>::execute(*p_generator, a, b);
}

}



/**
 * This class behaves like `std::uniform_int_distribution` and works around GCC
 * bug 80977, Clang bug 39209.
 */
template<typename UInt>
struct uniform_int_distribution
{
	static_assert(
		std::is_integral<UInt>::value, "Expected integer type"
	);
	static_assert(
		not std::is_signed<UInt>::value, "No signed integer implementation"
	);


	using result_type = UInt;


	struct param_type
	{
		param_type(UInt a, UInt b) :
			a_(a),
			b_(b)
		{
			assert(a <= b);
		}

		UInt a() const { return a_; }
		UInt b() const { return b_; }

		UInt a_;
		UInt b_;
	};


	uniform_int_distribution() : uniform_int_distribution(0) {}

	explicit uniform_int_distribution(
		UInt a,
		UInt b = std::numeric_limits<UInt>::max()
	) :
		param_(a, b)
	{
		if(a > b)
			throw std::range_error("Range is empty");
	}

	explicit uniform_int_distribution(const param_type& param) :
		param_(param)
	{}


	result_type a() const { return param_.a(); }
	result_type b() const { return param_.b(); }
	const param_type& param() const { return param_; }



	template<class Generator>
	result_type operator() (Generator& generator);


	param_type param_;
};



/**
 * This class checks if a generator outputs values in some interval `[0, 2^n)`.
 *
 * If `Generator::min()` and `Generator::max()` lack a `constexpr` qualifier,
 * then one must manually specialize this template. This is true for, e.g., the
 * Boost.Random number generators.
 */
template<typename Generator>
struct has_pow2_range
{
	using T_ = typename Generator::result_type;

	static constexpr bool value =
		std::is_integral<T_>::value
		and not std::is_signed<T_>::value
		and Generator::min() == 0
		and ispow2m1(Generator::max())
	;
};

}

#endif
