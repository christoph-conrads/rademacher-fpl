// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <algorithm>
#include <cassert>
#include <cinttypes> // strtoimax
#include <cstdarg> // vfprintf
#include <cstdio>
#include <cstdlib> // exit, setenv
#include <limits>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/random.hpp>
#include <random>
#include <type_traits> // is_signed
#include <unistd.h> // getopt
#include <vector>


using namespace rademacher_fpl::literals;

namespace rfpl = rademacher_fpl;


const char* g_program_name = nullptr;


void die(const char* fmt ...)
{
	va_list ap;
	va_start(ap, fmt);

	std::fprintf(stderr, "%s: ", g_program_name);
	std::vfprintf(stderr, fmt, ap);
	std::fprintf(stderr, "\n");

	va_end(ap);
	std::exit(EXIT_FAILURE);
}



enum class Status { OK, ERROR };

template<typename T>
Status parse_int(const std::string& str, T* p_result)
{
	assert(p_result);

	if(p_result == nullptr)
		return Status::ERROR;

	if(str.empty())
		return Status::ERROR;

	errno = 0;

	// always use signed integers because minus signs are silently parsed by
	// both `sscanf` and `strtoul`
	char* endptr = nullptr;
	auto base = 10;
	auto integer = std::strtoimax(str.c_str(), &endptr, base);

	if(*endptr != 0)
		return Status::ERROR;

	if(integer == std::numeric_limits<std::intmax_t>::max() and errno == ERANGE)
		return Status::ERROR;

	assert(errno == 0);

	if(integer < 0 and not std::is_signed<T>::value)
		return Status::ERROR;

	if(std::is_signed<T>::value)
	{
		// silence warnings about mixed-sign integer comparisons with GCC 5.4.0
		// or older when T is unsigned
		using S = typename std::make_signed<T>::type;

		constexpr auto MAX = std::numeric_limits<S>::max();
		constexpr auto MIN = std::numeric_limits<S>::min();

		if(integer > MAX or integer < MIN)
			return Status::ERROR;
	}
	else
	{
		assert(integer >= 0);

		auto u = std::uintmax_t(integer);

		if(u > std::numeric_limits<T>::max())
			return Status::ERROR;
	}

	*p_result = integer;

	return Status::OK;
}


void print_help(bool error_p)
{
	constexpr char HELP[] =
		"Usage: %1$s [-h] [-s <seed>] [-g <generator>]\n"
		"Options:\n"
		"  -h  Print help\n"
		"  -s  Set pseudo-random number generator seed\n"
		"  -g  Choose one of the random number generators \n"
		"      `mt19937_64`: 64-bit Mersenne Twister\n"
		"      `ranlux16`: 16-bit Ranlux\n"
		"      `ranlux32`: 32-bit Ranlux\n"
		"      `ranlux32_awc`: 32-bit Ranlux based on add-with-carry\n"
		"      `ranlux64`: 64-bit Ranlux\n"
		"      `fast_ranlux16`: fast 16-bit Ranlux\n"
		"      `fast_ranlux32`: fast 32-bit Ranlux\n"
		"      `fast_ranlux32_awc`: fast 32-bit Ranlux based on add-with-carry\n"
		"      `fast_ranlux64`: fast 64-bit Ranlux\n"
		"\n"
		"This program prints raw pseudo-random number generator values until\n"
		"killed.\n"
	;
	auto stream = error_p ? stderr : stdout;

// suppress warnings about C++11 not supporting %n$ operands
#if __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
#endif

	std::fprintf(stream, HELP, g_program_name);

#if __GNUC__
#pragma GCC diagnostic pop
#endif
}


template<typename Generator>
void print(unsigned seed)
{
	using T = typename Generator::result_type;

	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	static_assert(std::numeric_limits<T>::max() == Generator::max(), "");
	static_assert(std::numeric_limits<T>::min() == Generator::min(), "");

	constexpr auto N = 1_z << 20;
	auto gen = Generator(seed);
	auto xs = std::vector<T>(N);

	while(true)
	{
		for(auto& x : xs)
			x = gen();

		constexpr auto num_bytes = N * sizeof(T);
		auto ret = write(STDOUT_FILENO, xs.data(), num_bytes);

		if(ret < 0)
			die("write (ret=%d)", ret);

		auto retu = std::size_t(ret);

		if(retu != num_bytes)
			die("write (ret=%zu, num-bytes=%zu)", retu, num_bytes);
	}
}



int main(int argc, char** argv)
{
	::setenv("POSIXLY_CORRECT", "1", 0);

	g_program_name = argc > 0 ? argv[0] : "a.out";

	auto seed = 1u;
	auto generator = std::string("ranlux32_awc");

	constexpr char OPTSTRING[] = "hs:g:";

	auto ret = getopt(argc, argv, OPTSTRING);

	while(ret != -1)
	{
		auto argument = argv[optind-1];

		if(ret == 'h' or ret == '?')
		{
			auto error_p = ret == '?';

			print_help(error_p);
			std::exit(error_p ? EXIT_FAILURE : EXIT_SUCCESS);
		}
		else if(ret == 'g')
		{
			auto arg = std::string(argument);

			// omit the other PRNGs because they all return `uint_fastXX_t` so
			// when printing we would have to do some math to figure out many
			// random bytes they generate on each draw, e.g., `std::mt19937`
			// generates 32-bit values but  on a 64-bit system, this PRNG will
			// probably have `std::uint64_t` as result_type.
			if(arg != "mt19937_64"
				and arg != "ranlux16"
				and arg != "ranlux32"
				and arg != "ranlux64"
				and arg != "fast_ranlux16"
				and arg != "fast_ranlux32"
				and arg != "fast_ranlux64"
				and arg != "ranlux16_awc"
				and arg != "ranlux32_awc"
				and arg != "fast_ranlux16_awc"
				and arg != "fast_ranlux32_awc"
			)
			{
				die("unknown generator '%s'", argument);
			}

			generator = argument;
		}
		else if(ret == 's')
		{
			auto status = parse_int(argv[optind-1], &seed);

			if(status == Status::ERROR)
				die("cannot parse seed '%s' as unsigned integer", argument);
		}
		else
		{
			die("unknown option %d", ret);
		}

		ret = getopt(argc, argv, OPTSTRING);
	}


	std::fprintf(stderr, "rfpl-version    %s\n", RADEMACHER_FPL_VERSION);
	std::fprintf(stderr, "rfpl-git-commit %s\n", RADEMACHER_FPL_GIT_COMMIT);
	std::fprintf(stderr, "prng            %s\n", generator.c_str());
	std::fprintf(stderr, "seed            %u\n", seed);

	if(generator == "mt19937_64")
		print<std::mt19937_64>(seed);
	else if(generator == "ranlux16")
		print<rfpl::ranlux16>(seed);
	else if(generator == "ranlux32")
		print<rfpl::ranlux32>(seed);
	else if(generator == "ranlux64")
		print<rfpl::ranlux64>(seed);
	else if(generator == "fast_ranlux16")
		print<rfpl::fast_ranlux16>(seed);
	else if(generator == "fast_ranlux32")
		print<rfpl::fast_ranlux32>(seed);
	else if(generator == "fast_ranlux64")
		print<rfpl::fast_ranlux64>(seed);
	else if(generator == "ranlux16_awc")
		print<rfpl::ranlux16_awc>(seed);
	else if(generator == "ranlux32_awc")
		print<rfpl::ranlux32_awc>(seed);
	else if(generator == "fast_ranlux16_awc")
		print<rfpl::fast_ranlux16_awc>(seed);
	else if(generator == "fast_ranlux32_awc")
		print<rfpl::fast_ranlux32_awc>(seed);
}
