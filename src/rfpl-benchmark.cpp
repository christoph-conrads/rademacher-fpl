// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <algorithm>
#include <array>
#include <cassert>
#include <cinttypes> // strtoimax
#include <cstdarg> // vfprintf
#include <cstdio>
#include <cstdlib> // exit, setenv
#include <limits>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/random.hpp>
#include <rademacher-fpl/statistics.hpp>
#include <random>
#include <type_traits> // is_signed
#include <unistd.h> // getopt
#include <vector>


using namespace rademacher_fpl::literals;

namespace rfpl = rademacher_fpl;


const char* g_program_name = nullptr;


void die(const char* fmt ...)
{
	va_list ap;
	va_start(ap, fmt);

	std::fprintf(stderr, "%s: ", g_program_name);
	std::vfprintf(stderr, fmt, ap);
	std::fprintf(stderr, "\n");

	va_end(ap);
	std::exit(EXIT_FAILURE);
}



enum class Status { OK, ERROR };

template<typename T>
Status parse_int(const std::string& str, T* p_result)
{
	assert(p_result);

	if(p_result == nullptr)
		return Status::ERROR;

	if(str.empty())
		return Status::ERROR;

	errno = 0;

	// always use signed integers because minus signs are silently parsed by
	// both `sscanf` and `strtoul`
	char* endptr = nullptr;
	auto base = 10;
	auto integer = std::strtoimax(str.c_str(), &endptr, base);

	if(*endptr != 0)
		return Status::ERROR;

	if(integer == std::numeric_limits<std::intmax_t>::max() and errno == ERANGE)
		return Status::ERROR;

	assert(errno == 0);

	if(integer < 0 and not std::is_signed<T>::value)
		return Status::ERROR;

	if(std::is_signed<T>::value)
	{
		// silence warnings about mixed-sign integer comparisons with GCC 5.4.0
		// or older when T is unsigned
		using S = typename std::make_signed<T>::type;

		constexpr auto MAX = std::numeric_limits<S>::max();
		constexpr auto MIN = std::numeric_limits<S>::min();

		if(integer > MAX or integer < MIN)
			return Status::ERROR;
	}
	else
	{
		assert(integer >= 0);

		auto u = std::uintmax_t(integer);

		if(u > std::numeric_limits<T>::max())
			return Status::ERROR;
	}

	*p_result = integer;

	return Status::OK;
}


template<typename Real>
Status parse_float(const std::string& str, Real* p_result)
{
	assert(p_result);

	if(p_result == nullptr)
		return Status::ERROR;

	if(str.empty())
		return Status::ERROR;

	errno = 0;

	auto ptr = str.c_str();
	char* endptr = nullptr;
	auto f = std::strtold(ptr, &endptr);

	if(*endptr != 0)
		return Status::ERROR;

	if(f == std::numeric_limits<decltype(f)>::max() and errno == ERANGE)
		return Status::ERROR;

	assert(errno == 0);

	if(not std::isinf(f) and std::abs(f) > std::numeric_limits<Real>::max())
		return Status::ERROR;

	*p_result = f;

	return Status::OK;
}



void print_help(bool error_p)
{
	constexpr char HELP[] =
		"Usage: %1$s [-h] [-s <seed>] [-n <num-draws>] [-d <rfpl|std>]\n"
		"                           [-g <generator>] a b\n"
		"\n"
		"Options:\n"
		"  -h  Print help\n"
		"  -s  Set pseudo-random number generator seed\n"
		"  -n  Set number of draws\n"
		"  -d  Choose between the uniform real distribution from the C++11\n"
		"      standard library (`std`) and the implementation in this\n"
		"      package (`rfpl`)\n"
		"  -g  Choose one of the random number generators \n"
		"      `mt19937`: 32-bit Mersenne Twister\n"
		"      `mt19937_64`: 64-bit Mersenne Twister\n"
		"      `ranlux24`: Ranlux with luxury level 3\n"
		"      `ranlux48`: Ranlux with luxury level 4\n"
		"\n"
		"This program draws random uniformly distributed single precision\n"
		"floating-point values in the interval [a, b) and prints\n"
		"statistics about them.\n"
		"\n"
		"Examples:\n"
		"  %1$s -- -1 2\n"
		"  %1$s -s 1 -n 100 -d rfpl -g mt19937 -- -1 2\n"
		"  %1$s -s 1 -n 100 -d rfpl -g mt19937 -- -1.0e0 +2.0e0\n"
		"  %1$s -s 1 -n 100 -d rfpl -g mt19937 -- 1 127 0 0 128 0\n"
	;
	auto stream = error_p ? stderr : stdout;

// suppress warnings about C++11 not supporting %n$ operands
#if __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
#endif

	std::fprintf(stream, HELP, g_program_name);

#if __GNUC__
#pragma GCC diagnostic pop
#endif
}


template<typename Generator, typename Distribution>
void benchmark(std::uintmax_t num_draws, unsigned seed, Distribution dist)
{
	static_assert(
		std::is_floating_point<typename Distribution::result_type>::value, ""
	);

	auto gen = Generator(seed);

	for(auto i = 0_u; i < 1000_u; ++i)
		(void) dist(gen);

	auto t_0 = clock();

	for(auto i = 0_u; i < num_draws; ++i)
		(void) dist(gen);

	auto t_1 = clock();
	auto t_sec = 1.0 * (t_1 - t_0) / CLOCKS_PER_SEC;

	std::printf("time-sec %.1f\n", t_sec);
}



int main(int argc, char** argv)
{
	using Real = float;

	::setenv("POSIXLY_CORRECT", "1", 0);

	g_program_name = argc > 0 ? argv[0] : "a.out";

	auto seed = 1u;
	auto num_draws = 100_u;
	auto distribution = std::string("rfpl");
	auto generator = std::string("mt19937");

	constexpr char OPTSTRING[] = "hs:n:d:g:";

	auto ret = getopt(argc, argv, OPTSTRING);

	while(ret != -1)
	{
		auto argument = argv[optind-1];

		if(ret == 'h' or ret == '?')
		{
			auto error_p = ret == '?';

			print_help(error_p);
			std::exit(error_p ? EXIT_FAILURE : EXIT_SUCCESS);
		}
		else if(ret == 'd')
		{
			if(std::string(argument) != "rfpl" and std::string(argument)!="std")
				die("unknown distribution '%s', use `rfpl` or `std`", argument);

			distribution = argument;
		}
		else if(ret == 'g')
		{
			auto arg = std::string(argument);

			if(arg != "mt19937" and arg != "mt19937_64"
				and arg != "ranlux24" and arg != "ranlux48"
			)
			{
				die("unknown generator '%s',"
					"use `mt19937`, `mt19937_64`, `ranlux24`, or `ranlux48`",
					argument
				);
			}

			generator = argument;
		}
		else if(ret == 'n')
		{
			auto status = parse_int(argv[optind-1], &num_draws);

			if(status == Status::ERROR)
				die("cannot parse the number of draws '%s' as unsigned integer",
					argument
				);
		}
		else if(ret == 's')
		{
			auto status = parse_int(argv[optind-1], &seed);

			if(status == Status::ERROR)
				die("cannot parse seed '%s' as unsigned integer", argument);
		}

		ret = getopt(argc, argv, OPTSTRING);
	}

	auto num_remaining_args = argc - optind;

	if(num_remaining_args != 2 and num_remaining_args != 6)
		die("expected two or six trailing arguments");

	auto a = std::numeric_limits<Real>::quiet_NaN();
	auto b = std::numeric_limits<Real>::quiet_NaN();

	if(num_remaining_args == 2)
	{
		if(parse_float(argv[optind], &a) == Status::ERROR)
			die("error when parsing '%s' as float", argv[optind]);

		if(parse_float(argv[optind+1], &b) == Status::ERROR)
			die("error when parsing '%s' as float", argv[optind+1]);
	}
	else if(num_remaining_args == 6)
	{
		auto signbit_a = 0;
		auto exponent_a = 0u;
		auto significand_a = 0u;

		if(parse_int(argv[optind+0], &signbit_a) == Status::ERROR)
			die("cannot parse '%s' as sign bit", argv[optind+0]);

		if(parse_int(argv[optind+1], &exponent_a) == Status::ERROR)
			die("cannot parse '%s' as unsigned exponent", argv[optind+1]);

		if(parse_int(argv[optind+2], &significand_a) == Status::ERROR)
			die("cannot parse '%s' as significand", argv[optind+2]);

		if(signbit_a != 0 and signbit_a != 1)
			die("signbit of a must be zero or one (signbit=%d)", signbit_a);
		if(exponent_a > 255u)
			die("exponent of a is too large (exponent=%u)", exponent_a);
		if(significand_a >= 1u << 23)
			die("significand of a is too large (significand=%u)",significand_a);


		auto signbit_b = 0u;
		auto exponent_b = 0u;
		auto significand_b = 0u;

		if(parse_int(argv[optind+3], &signbit_b) == Status::ERROR)
			die("cannot parse '%s' as sign bit", argv[optind+3]);

		if(parse_int(argv[optind+4], &exponent_b) == Status::ERROR)
			die("cannot parse '%s' as unsigned exponent", argv[optind+4]);

		if(parse_int(argv[optind+5], &significand_b) == Status::ERROR)
			die("cannot parse '%s' as significand", argv[optind+5]);

		if(signbit_b != 0u and signbit_b != 1u)
			die("signbit of b must be zero or one (signbit=%d)", signbit_b);
		if(exponent_b > 255u)
			die("exponent of b is too large (exponent=%u)", exponent_b);
		if(significand_b >= 1u << 23u)
			die("significand of b is too large (significand=%u)",significand_b);

		a = rfpl::assemble_like(a, signbit_a, exponent_a, significand_a);
		b = rfpl::assemble_like(b, signbit_b, exponent_b, significand_b);
	}

	if(not std::isfinite(a))
		die("a must be a finite real number (a=%.2e)", a);

	if(std::isnan(b))
		die("b must be a real number (b=%.2e)", b);

	if(a >= b)
		die("a must be strictly less than b (a=%.2e, b=%.2e)", a, b);


	std::printf("RFPL benchmark\n");
	std::printf("version %s\n", RADEMACHER_FPL_VERSION);
	std::printf("git-commit %s\n", RADEMACHER_FPL_GIT_COMMIT);
	std::printf("num-draws %ju\n", num_draws);
	std::printf("seed %u\n", seed);
	std::printf("generator %s\n", generator.c_str());
	std::printf("a %+15.9e\n", a);
	std::printf("b %+15.9e\n", b);

	if(distribution == "rfpl")
	{
		auto dist = rfpl::uniform_real_distribution<Real>(a, b);

		if(generator == "mt19937")
			benchmark<std::mt19937>(num_draws, seed, dist);
		else if(generator == "mt19937_64")
			benchmark<std::mt19937_64>(num_draws, seed, dist);
		else if(generator == "ranlux24")
			benchmark<std::ranlux24>(num_draws, seed, dist);
		else if(generator == "ranlux48")
			benchmark<std::ranlux48>(num_draws, seed, dist);
	}
	else if(distribution == "std")
	{
		auto dist = std::uniform_real_distribution<Real>(a, b);

		if(generator == "mt19937")
			benchmark<std::mt19937>(num_draws, seed, dist);
		else if(generator == "mt19937_64")
			benchmark<std::mt19937_64>(num_draws, seed, dist);
		else if(generator == "ranlux24")
			benchmark<std::ranlux24>(num_draws, seed, dist);
		else if(generator == "ranlux48")
			benchmark<std::ranlux48>(num_draws, seed, dist);
	}
}
