// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <rademacher-fpl/impl/statistics.hpp>

namespace rademacher_fpl {

template struct draw_counter<float, std::uint64_t>;
template struct draw_counter<float, std::uint32_t>;

template
std::uintmax_t compute_num_virtual_bins(float, float);

template
double compute_kolmogorov_smirnov_statistic(const draw_counter<float,std::uint32_t>&);
template
double compute_kolmogorov_smirnov_statistic(const draw_counter<float,std::uint64_t>&);

template
float compute_ks_rejection_value(float, std::uint64_t, std::uint64_t);
template
double compute_ks_rejection_value(double, std::uint64_t, std::uint64_t);

}
