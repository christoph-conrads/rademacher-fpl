// Copyright 2018-2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <rademacher-fpl/binary8.hpp>
#include <rademacher-fpl/literals.hpp>
#include <rademacher-fpl/math.hpp>

#include <cstdio>


using namespace rademacher_fpl::literals;


namespace rademacher_fpl {

bool operator!= (binary8 l, binary8 r)
{
	if(isnan(l) or isnan(r))
		return false;

	return not (l == r);
}


bool operator== (binary8 l, binary8 r)
{
	if(isnan(l) or isnan(r))
		return false;

	if(l.exponent_ == 0 and r.exponent_ == 0 and
		l.fraction_ == 0 and r.fraction_ == 0
	)
		return true;

	return
		l.sign_ == r.sign_ and
		l.exponent_ == r.exponent_ and
		l.fraction_ == r.fraction_;
}


bool operator< (binary8 l, binary8 r)
{
	if(isnan(l) or isnan(r))
		return false;

	if(l == r)
		return false;

	if(l.sign_ == 0 and r.sign_ == 0)
	{
		if(l.exponent_ < r.exponent_)
			return true;
		if(l.exponent_ == r.exponent_ and l.fraction_ <= r.fraction_)
			return true;

		return false;
	}

	if(l.sign_ == 1 and r.sign_ == 1)
	{
		assert(l != r);
		return -r < -l;
	}

	assert(l.sign_ != r.sign_);

	return l.sign_ == 1 and r.sign_ == 0;
}


binary8 assemble_like(binary8, bool s, std::uint8_t e, std::uint8_t f)
{
	return binary8(s, e, f);
}



std::string to_string(binary8 f)
{
	constexpr char FORMAT[] = "%+de%+03d\n";
	char buffer[20] = { 0 };

	auto s = signbit(f) == 0 ? int{significand(f)} : -int{significand(f)};
	auto e = int{exponent(f)};

	std::snprintf(buffer, sizeof(buffer), FORMAT, s, e);

	return buffer;
}

}
