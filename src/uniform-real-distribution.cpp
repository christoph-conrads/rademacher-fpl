// Copyright 2018 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <rademacher-fpl/impl/uniform-int-distribution.hpp>
#include <rademacher-fpl/impl/uniform-real-distribution.hpp>
#include <random>


namespace rademacher_fpl
{
	template struct uniform_real_distribution<float>;
	template float uniform_real_distribution<float>::operator() (std::minstd_rand0&);
	template float uniform_real_distribution<float>::operator() (std::minstd_rand&);
	template float uniform_real_distribution<float>::operator() (std::mt19937&);
	template float uniform_real_distribution<float>::operator() (std::mt19937_64&);
	template float uniform_real_distribution<float>::operator() (std::ranlux24_base&);
	template float uniform_real_distribution<float>::operator() (std::ranlux48_base&);
	template float uniform_real_distribution<float>::operator() (std::ranlux24&);
	template float uniform_real_distribution<float>::operator() (std::ranlux48&);
	template float uniform_real_distribution<float>::operator() (std::knuth_b&);
	template float uniform_real_distribution<float>::operator() (std::random_device&);

	template struct uniform_real_distribution<double>;
	template double uniform_real_distribution<double>::operator() (std::minstd_rand0&);
	template double uniform_real_distribution<double>::operator() (std::minstd_rand&);
	template double uniform_real_distribution<double>::operator() (std::mt19937&);
	template double uniform_real_distribution<double>::operator() (std::mt19937_64&);
	template double uniform_real_distribution<double>::operator() (std::ranlux24_base&);
	template double uniform_real_distribution<double>::operator() (std::ranlux48_base&);
	template double uniform_real_distribution<double>::operator() (std::ranlux24&);
	template double uniform_real_distribution<double>::operator() (std::ranlux48&);
	template double uniform_real_distribution<double>::operator() (std::knuth_b&);
	template double uniform_real_distribution<double>::operator() (std::random_device&);
}
