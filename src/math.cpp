// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#include <rademacher-fpl/impl/math.hpp>

namespace rademacher_fpl {

template float nextafter_0(float, float);
template double nextafter_0(double, double);


template traits::as_unsigned_t<float> exponent(float);
template traits::as_unsigned_t<double> exponent(double);

template traits::as_unsigned_t<float> significand(float);
template traits::as_unsigned_t<double> significand(double);


template struct assembler<float>;
template struct assembler<double>;



template
typename traits::as_unsigned<float>::type count_num_values_in(float, float);
template
typename traits::as_unsigned<double>::type count_num_values_in(double,double);

}
