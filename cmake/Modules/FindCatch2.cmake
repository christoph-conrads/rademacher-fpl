# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set(Catch2_ROOT "" CACHE PATH "Path to Catch2 root directory")


find_path(
	Catch2_INCLUDE_DIRS
	NAMES catch2/catch.hpp
	PATHS ${Catch2_ROOT}
	PATH_SUFFIXES include
)


# get Catch2 version if we found the Catch2 header
if(Catch2_INCLUDE_DIRS)

	foreach(_catch2_key "MAJOR" "MINOR" "PATCH")
		set(_catch2_version_regexp "^#define CATCH_VERSION_${_catch2_key} ([0-9]+)$")

		file(STRINGS "${Catch2_INCLUDE_DIRS}/catch2/catch.hpp" _catch2_match
			LIMIT_INPUT 1000
			REGEX "${_catch2_version_regexp}"
		)

		string(
			REGEX REPLACE "${_catch2_version_regexp}" "\\1"
			_catch2_version
			${_catch2_match}
		)

		set(_catch2_version_variable "Catch2_VERSION_${_catch2_key}")
		set("${_catch2_version_variable}" "${_catch2_version}")
	endforeach()

	set(Catch2_VERSION
		"${Catch2_VERSION_MAJOR}.${Catch2_VERSION_MINOR}.${Catch2_VERSION_PATCH}"
	)
endif()


include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
	Catch2
	VERSION_VAR Catch2_VERSION
	REQUIRED_VARS Catch2_INCLUDE_DIRS
)
