if [ "$(lsb_release --id --short)" = 'Ubuntu' ]
then
	apt-get install \
		build-essential \
		cmake \
		git \
		wget
fi
