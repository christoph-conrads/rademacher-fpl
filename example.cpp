#include <cstdio>
#include <rademacher-fpl/uniform-real-distribution.hpp>
#include <random>


int main()
{
	auto gen = std::mt19937();
	auto dist = rademacher_fpl::uniform_real_distribution<float>();
	auto x = dist(gen);

	std::printf("x = %16.10e\n", x);
	std::printf("The Rademacher Floating Point Library works.\n");
}
