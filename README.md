# Rademacher Floating Point Library

## About

The Rademacher Floating Point Library (RFPL) provides a C++11 class computing random uniformly distributed floating-point values in a given half-open interval `[a,b)`. In comparison to every other existing software, this library

* draws randomly from *all* floating-point values in range,
* computes values that are actually uniformly distributed,
* avoids round-off errors causing out-of-range values,
* uses only integer arithmetic, and
* works with built-in C++ float types as well with custom floating-point types.

Moreover, this library can be used with custom float types by
* providing template specializations for the functions `exponent`, `significand`, `signbit`, and `assemble_like` in `include/rademacher-fpl/math.hpp`,
* providing comparison operators, e.g., `operator<`, and
* specializing `std::numeric_limits`.

See `include/rademacher-fpl/binary8.hpp` for an example.

Functions computing the Kolmogorov-Smirnov statistic for a set of draws can be found in `include/rademacher-fpl/statistics.hpp`. A `uniform_int_distribution` implementation without [GCC bug 80977](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80977) and without [LLVM bug 39209](https://bugs.llvm.org/show_bug.cgi?id=39209) can be found in `include/rademacher-fpl/uniform-int-distribution.hpp`.


RFPL is free software ("free" as in "free beer" and "free" as in "freedom") and licensed under the Mozilla Public License 2.0.


## Installation

Ensure at CMake 3.0 or newer and GNU Make are installed on your system. If you want to build the tests, then you also must have Catch2 2.5.0 or newer available. Decide on an installation directory, locate the source directory of this library, open a shell, and navigate to the RFPL source directory. Create a build directory in the current working directory by typing
```sh
mkdir -v build
```
Run CMake and pass the installation directory on the command line:
```sh
cmake \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=<INSTALLATION-FOLDER> \
	-- ..
```
Build the software
```sh
make
```
If the test framework Catch2 is installed on your computer and if CMake found it, you can run the tests by executing
```sh
make test
```
Finally, install RFPL:
```sh
make install
```


## Usage

To compute uniformly distributed random floats in C++11 add the following include to your code:
```cpp
#include <rademacher-fpl/uniform-real-distribution.hpp>
```
Instantiate `rademacher_fpl::uniform_real_distribution` and call it like its C++11 standard library clone:
```cpp
	auto gen = std::mt19937();
	auto dist = rademacher_fpl::uniform_real_distribution<float>(1, 2);
	auto x = dist(gen);

	std::printf("x=%f\n", x);
```
Compile the code and link against the shared library:
```sh
g++ -lrademacher-fpl code.o
```
To use RFPL as a header-only library, modify the include as follows:
```cpp
#include <rademacher-fpl/impl/uniform-real-distribution.hpp>
```

Depending on the interval, the uniform real distribution in this library may be slightly faster or significantly slower than the C++11 standard library implementations. If you decide to use this library, the author suggests to use a good random number generator returning 32-bit or 64-bit integers, e.g., `std::mt19937` or `std::mt19937_64`. This decision will come at (almost) no additional cost because the algorithms in this library often draw values from intervals `[0, 2^p)` and drawing these values is much easier and faster with a Mersenne Twister than with a, say, linear congruential generator (`std::minstd_rand` is such a generator).



## Compatibility

RFPL was built and tested with Catch 2.5.0 on the following platforms:
* GCC 7.3.0, Clang 6.0.1, libstdc++, libc++ on x86\_64-linux-gnu,
* GCC 5.4.0, Clang 3.8.0, libstdc++, libc++ on Ubuntu 16.04 on i686.

The software should work with any standards-compliant C++11 compiler.



## Namesake

In mid-2016 the RFPL author was developing an eigensolver for algebraic generalized eigenvalues problems requiring random vectors whose entries had mean zero and variance one for a Hutchinson trace estimator. Due to the blog post highlighting that the C++11 uniform real distribution was broken by design (see the acknowledgements below), the RFPL author decided to draw values from the Rademacher distribution instead of the normal distribution as in the literature because the Rademacher distribution had much better engineering properties.

The Rademacher distribution returns values +1, -1, each with probability 0.5.  Drawing from the distribution requires only a single random bit and values can be used for generating integers and floating-point data alike. Its computation is therefore fast and fool-proof. Among all distributions with mean zero and variance one, the Rademacher distribution is probably the most desirable one from the point of view of engineering.

Hans Adolph Rademacher was a mathematician working in number theory, real and complex analysis, measure theory, geometry, and numerical analysis. He was born 1892 in Hamburg, Germany, he served from 1914 to 1916 in the German armed forces before finishing his doctoral dissertation "Eineindeutige Abbildungen und Meßbarkeit" at the University of Göttigen also in 1916; his supervisor was Constantin Carathéodory. In 1925, he became professor in Breslau until 1934, when he was let go due to his pacifism. From 1935 until his retirement in 1962 he worked at the University of Pennsylvania. He died 1969 in Pennsylvania.  Rademacher was a founding editor of the journal Acta Arithmetica in 1935 which released an epitaph: [Hans Rademacher (1892-1969)](http://matwbn.icm.edu.pl/ksiazki/aa/aa61/aa6131.pdf).



## Acknowledgements

The RFPL author's attention was called to the sorry state of uniform random float generators in mid-2016 by Artur Grabowski's blog post [*State of the art of random double generation.*](http://www.blahonga.org/2015/04/state-of-art-of-random-double-generation.html) from 2015. Artur Grabowski had correctly identified the following issues in common C++11 `std::uniform_real_distribution` implementations:

* round-off errors create out-of-range values,
* creating random real values in `[a,b)` by computing `(b-a) * [0,1) + a` does not work with floating-point arithmetic,
* the calculated values do not represent a uniform distribution, and
* all of these issues are codified in the C++11 standard.
